<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table border="1">
    <tr>
        <th>Login</th>
        <th>Password</th>
        <th>Name</th>
        <th></th>
        <th></th>
    </tr>

<c:forEach items="${users}" var="user">
    <tr>
        <td>${user.login}</td>
        <td>${user.password}</td>
        <td>${user.name}</td>
        <td><a href="/Bugtracker_Hibernate/loadUser.html?id=${user.id}">Edit</a></td>
        <td style="color: red;"><a href="/Bugtracker_Hibernate/deleteUser.html?id=${user.id}">Delete</a></td>

    </tr>
</c:forEach>
</table>

<br><a href="/Bugtracker_Hibernate/loadUser.html">Create New User</a>
<br><a href="/Bugtracker_Hibernate/index.html">Main Page</a>
