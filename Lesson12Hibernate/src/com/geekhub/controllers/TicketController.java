package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.TicketPriority;
import com.geekhub.beans.TicketStatus;
import com.geekhub.services.TicketDAO;
import com.geekhub.services.UserDAO;

@Controller
public class TicketController {
	
	@Autowired TicketDAO ticketDAO;
	@Autowired UserDAO userDAO;
	
	@RequestMapping("/listTickets.html")
	public String list(ModelMap map) {
		map.put("tickets", ticketDAO.getTicketList());
		return "tickets";
	}
	
	@RequestMapping("/loadTicket.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		Ticket ticket = id == null ? new Ticket() : ticketDAO.getTicketById(id);
		TicketStatus[] ticketStatus = TicketStatus.values(); 
        TicketPriority[] ticketPriority = TicketPriority.values();
        map.put("ticket", ticket);
        map.put("users", userDAO.getUserList()); 
        map.put("status", ticketStatus); 
        map.put("priority", ticketPriority); 
		return "ticket";
	}
	
	@RequestMapping("/deleteTicket.html")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		ticketDAO.deleteTicket(id);
		return "redirect:listTickets.html";
	}
	
	@RequestMapping("/saveTicket.html")
	public String save(Ticket ticket) {
		ticketDAO.saveTicket(ticket);
		return "redirect:./listTickets.html";
	}

}
