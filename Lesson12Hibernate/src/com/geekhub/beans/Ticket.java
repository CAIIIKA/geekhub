package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.lang.Integer;


@Entity
@Table(name="TICKET")
public class Ticket {
	
	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="TITLE")
    private String title;
    
	@Column(name="DESCRIPTION")
    private String description;
	
	@ManyToOne
	@JoinColumn(name="OWNERID")
    private User ownerId;
    
	@Column(name="STATUS")
    private TicketStatus status;
    
	@Column(name="PRIORITY")
    private TicketPriority priority;
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public User getOwner() {
        return ownerId;
    }
    
    public void setOwner(User ownerId) {
        this.ownerId = ownerId;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    public TicketPriority getPriority() {
        return priority;
    }

    public void setPriority(TicketPriority priority) {
		this.priority = priority;
	}
}
