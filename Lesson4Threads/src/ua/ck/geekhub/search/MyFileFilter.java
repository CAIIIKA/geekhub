package ua.ck.geekhub.search;

import java.io.File;
import java.io.FileFilter;

public class MyFileFilter implements FileFilter {

	private String pattern;

	MyFileFilter(String pattern) {
		this.pattern = pattern;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@Override
	public boolean accept(File file) {
		return file.isFile() && file.getName().endsWith(pattern);
	}
}
