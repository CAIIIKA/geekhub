package ua.ck.geekhub.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public class Test {

	public static void find(File path, String pattern) {
		FileFilter folderFilter = new FolderFilter();
		MyThread1 th = new MyThread1(path, pattern);
		th.start();
		File[] dirs = path.listFiles(folderFilter);
		for (int i = 0; i < dirs.length; i++) {
			find(dirs[i], pattern);
		}

	}

	public static void main(String[] args) throws IOException,
			InterruptedException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Input folder :");
		String dir = br.readLine();
		System.out.print("Input pattern: ");
		String pattern = br.readLine();
		File path = new File(dir);
		long time1 = System.currentTimeMillis();
		find(path, pattern);

		if (!MyThread1.getAllStackTraces().isEmpty())
			Thread.sleep(0);

		long time2 = System.currentTimeMillis();
		System.out.print(time2 - time1);
	}

}
