package ua.ck.geekhub.search;

import java.io.File;
import java.io.FileFilter;

public class MyThread1 extends Thread {

	File path;
	String pattern;

	public MyThread1(File path, String pattern) {
		this.path = path;
		this.pattern = pattern;
	}

	public MyThread1() {
	}

	public void setPath(File path) {
		this.path = path;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void run() {
		FileFilter filter = new MyFileFilter(pattern);
		File[] files = path.listFiles(filter);
		for (int i = 0; i < files.length; i++)
			System.out.print(files[i] + "\n");
	}

}
