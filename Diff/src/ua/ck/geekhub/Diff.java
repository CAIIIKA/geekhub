package ua.ck.geekhub;
public class Diff {

    static char[]  a  = "abcdfghjqz".toCharArray();
    static char[]  b  = " abcdefgijkrxyz".toCharArray();
    static int     m       = a.length;
    static int     n       = b.length;


    public static int[][] lcsLength(char[] a, char[] b) {
        int[][] tmp = new int[m][n];
        for (int i = 1; i < a.length; i++) {
            for (int j = 1; j < b.length; j++) {
                if (a[i] == b[j]) {
                    tmp[i][j] = tmp[i - 1][j - 1] + 1;
                } else {
                    tmp[i][j] = Math.max(tmp[i][j - 1], tmp[i - 1][j]);
                }
            }
        }
        return tmp;
    }

    public static void diff(int tmp[][], char a[], char b[], int i,
            int j) {
        if ((i >= 0) && (j >= 0) && (a[i] == b[j])) {
            diff(tmp, a, b, i - 1, j - 1);
            System.out.print(a[i]);
        } else if ((j > 0) && ((i == 0) || (tmp[i][j - 1] >= tmp[i - 1][j]))) {
            diff(tmp, a, b, i, j - 1);
            System.out.print("(" + b[j] + ")");
        } else if ((i > 0) && ((j == 0) || (tmp[i][j - 1] < tmp[i - 1][j]))) {
            diff(tmp, a, b, i - 1, j);
            System.out.print("[" + a[i] + "]");
        } else {
            System.out.print("");
        }
    }

    public static void main(String[] args) {
    	int[][] opt = lcsLength(a, b);
        diff(opt, a, b, m - 1, n - 1);
    }
}

