package ua.ck.geekhub.dz2;

public class Vote {
	private String name;

	public Vote(String name) {
		this.name = name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name + " ";
	}

}
