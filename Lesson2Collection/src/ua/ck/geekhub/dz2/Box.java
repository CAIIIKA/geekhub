package ua.ck.geekhub.dz2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Box implements Collection<Vote> {

	List<Vote> box;

	public Box() {
		box = new ArrayList<Vote>();
	}

	@Override
	public boolean add(Vote vote) {
		if (vote.getName().equals("#1"))
			return false;
		if (vote.getName().equals("#2"))
			if (!box.add(vote))
				return false;
		return box.add(vote);
	}

	@Override
	public boolean addAll(Collection<? extends Vote> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		box.clear();

	}

	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<Vote> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString() {
		return box.toString();
	}

}
