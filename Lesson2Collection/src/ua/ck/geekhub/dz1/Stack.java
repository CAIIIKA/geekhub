package ua.ck.geekhub.dz1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

public class Stack implements Queue {
	List lst;

	Stack() {
		lst = new ArrayList();
	}

	@Override
	public boolean addAll(Collection arg0) {
		return lst.addAll(arg0);
	}

	@Override
	public void clear() {
		lst.clear();
	}

	@Override
	public boolean contains(Object arg0) {
		return lst.contains(arg0);
	}

	@Override
	public boolean containsAll(Collection arg0) {
		return lst.containsAll(arg0);
	}

	@Override
	public boolean isEmpty() {
		return lst.isEmpty();
	}

	@Override
	public Iterator iterator() {
		return lst.iterator();
	}

	@Override
	public boolean remove(Object arg0) {
		return lst.remove(arg0);
	}

	@Override
	public boolean removeAll(Collection arg0) {
		return lst.removeAll(arg0);
	}

	@Override
	public boolean retainAll(Collection arg0) {
		return lst.retainAll(arg0);
	}

	@Override
	public int size() {
		return lst.size();
	}

	@Override
	public Object[] toArray() {
		return lst.toArray();
	}

	@Override
	public Object[] toArray(Object[] arg0) {
		return lst.toArray(arg0);
	}

	@Override
	public boolean add(Object arg0) {
		return lst.add(arg0);
	}

	@Override
	public Object element() {
		return lst.get(lst.size() - 1);
	}

	@Override
	public boolean offer(Object arg0) {
		return (lst.add(arg0));
	}

	@Override
	public Object peek() {
		if (lst.size() == 0)
			return null;
		return lst.get(lst.size() - 1);
	}

	@Override
	public Object poll() {
		if (lst.size() == 0)
			return null;
		Object obj = lst.get(lst.size() - 1);
		lst.remove(lst.size() - 1);
		return obj;
	}

	@Override
	public Object remove() {
		Object obj = lst.get(lst.size() - 1);
		lst.remove(lst.size() - 1);
		return obj;
	}

}
