package ua.ck.geekhub.dz1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Test {
	public static void printStack(Stack st) {
		Iterator itr = st.iterator();

		while (itr.hasNext()) {
			Object element = itr.next();
			System.out.print(element + " ");
		}
	}

	public static void main(String[] args) {
		Stack st = new Stack();

		st.add(5);
		st.add("ee");
		st.add(9);
		st.add("gg");

		printStack(st);
		System.out.print("\n" + st.poll() + "\n");
		printStack(st);
		System.out.print("\n" + st.peek() + "\n");
		printStack(st);
	}

}
