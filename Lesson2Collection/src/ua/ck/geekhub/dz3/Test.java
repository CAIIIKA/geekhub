package ua.ck.geekhub.dz3;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class Test {
	public static void main(String[] args) {
		List<Car> lst = new ArrayList<Car>();
		lst.add(new Car("BMW", 10, "red"));
		lst.add(new Car("BMW", 10, "black"));
		lst.add(new Car("Opel", 5, "red"));
		lst.add(new Car("Opel", 8, "red"));
		lst.add(new Car("Mersedes", 10, "black"));
		lst.add(new Car("Mersedes", 10, "red"));
		lst.add(new Car("Mersedes", 10, "white"));
		Collections.sort(lst);
		System.out.print(lst);
	}
}
