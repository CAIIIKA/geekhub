package ua.ck.geekhub.dz3;

public class Car implements Comparable<Car> {
	private String mark;
	private int longBase;
	private String color;

	public Car(String mark, int longBase, String color) {
		this.mark = mark;
		this.longBase = longBase;
		this.color = color;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public void setLongBase(int longBase) {
		this.mark = mark;
	}

	public void setColor(String color) {
		this.mark = mark;
	}

	public String getMark() {
		return mark;
	}

	public int compareTo(Car car) {
		if (!color.equals(car.color))
			return color.compareTo(car.color);
		if (!mark.equals(car.mark))
			return mark.compareTo(car.mark);
		if (longBase < car.longBase)
			return 1;
		if (longBase > car.longBase)
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return mark + " " + longBase + " " + color + "\n";
	}

}
