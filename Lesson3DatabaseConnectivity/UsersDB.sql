SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


CREATE DATABASE `UsersDB` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `UsersDB`;


CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `patronymic` varchar(20) NOT NULL,
  `age` int(3) NOT NULL,
  `country` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

