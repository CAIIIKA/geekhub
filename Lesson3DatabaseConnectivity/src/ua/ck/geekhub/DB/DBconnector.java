//work with any database
package ua.ck.geekhub.DB;

import java.io.*;
import java.sql.*;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class DBconnector {

	public static void main(String[] args) throws SQLException, IOException {
		BufferedReader d = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Input host(for example 127.0.0.1:3306): ");
		String url = "jdbc:mysql://" + d.readLine() + "/";
		System.out.print("Input database name: ");
		String dbName = d.readLine();
		System.out.print("Input username: ");
		String userName = d.readLine();
		System.out.print("Input password: ");
		String password = d.readLine();
		/*
		 * String url = "jdbc:mysql://127.0.0.1:3306/"; 
		 * String dbName ="UsersDB"; 
		 * String userName = "root"; 
		 * String password = "";
		 */

		Connection conn = (Connection) DriverManager.getConnection(
				url + dbName, userName, password);
		Statement statement = (Statement) conn.createStatement();

		System.out.print("Input request(q for exit): ");
		String req = d.readLine();
		while (!req.equals("q")) {
			Request rq = new Request(req);
			// SELECT action
			if (rq.getAction().equalsIgnoreCase("select")) {
				rq.selectFromTable(statement);
			}
			// other actions
			else {
				rq.otherAction(statement);
			}
			req = d.readLine();
		}
		d.close();
		conn.close();

	}
}
