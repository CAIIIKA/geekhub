package ua.ck.geekhub.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.mysql.jdbc.Statement;
import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;

public class Request {
	private String req;

	public Request(String req) {
		this.req = req;
	}

	public String getRequest() {
		return req;
	}

	public void setRequest(String req) {
		this.req = req;
	}

	// SELECT
	public void selectFromTable(Statement statement) {
		String sql = req;
		try {
			ResultSet result = statement.executeQuery(sql);
			printFormatedTable(result);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	// other actions
	public void otherAction(Statement statement) {
		try {
			int a = statement.executeUpdate(req);
			System.out.println("Total modified: " + a);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

	// print table
	private void printFormatedTable(ResultSet result) throws SQLException {
		int k = 1;
		int count = 0;
		List<String> lst = new ArrayList<String>();

		// get names of collums
		for (int i = 1; i <= result.getMetaData().getColumnCount(); i++) {
			lst.add(result.getMetaData().getColumnName(i));
		}

		Iterator it = lst.iterator();

		while (it.hasNext()) {
			String element = (String) it.next();
			System.out.print(element);
			// for 1 to length of collum - leng of text in the collum
			for (int j = 1; j <= result.getMetaData().getColumnDisplaySize(k)
					- element.length(); j++)
				System.out.print(" ");
			count += result.getMetaData().getColumnDisplaySize(k) + 1;
			k++;
			System.out.print("|");
		}
		System.out.println();
		// ---------------- in the table
		for (int t = 1; t < count; t++)
			System.out.print("-");
		System.out.println("|");

		while (result.next()) {
			for (int i = 0; i < result.getMetaData().getColumnCount(); i++) {
				System.out.print(result.getString(lst.get(i)));
				// for 1 to length of collum - length of the text in the collum
				for (int j = 1; j <= result.getMetaData().getColumnDisplaySize(
						i + 1)
						- result.getString(lst.get(i)).length(); j++)
					System.out.print(" ");
				System.out.print("|");
			}
			System.out.print("\n");
		}
	}

	// get current action
	public String getAction() {
		if (req.indexOf(' ') != -1)
			return req.substring(0, req.indexOf(' '));
		else
			return req;

	}
}
