package ua.ck.geekhub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



public class ParseDeals {

	public static void main(String[] args) {

		Document doc;
		try {
			doc = Jsoup
					.connect(
							"http://flyer.harristeeter.com/HT_eVIC/ThisWeek/SelectStore.jsp?lm=1")
					.data("SELECTED_STORE", "336")
					.data("BUTTON_CONTINUE", "Continue").post();
			Elements tables = doc.select("table[border=0] [cellspacing=0][width=100%][cellpadding=0][height=100%]");
		
			List <Deal> deals = new ArrayList<Deal>();
			
			for (Element table: tables){
				Deal deal = new Deal();
				Elements trs = table.select("tr");
				String additionalInf = trs.get(0).select("td").select("font[size=1]").text();
				deal.setAdditionalInformation(additionalInf);
				String nameAndAdditional = trs.get(0).select("td").select("font[size=2]").text();
				String name;
				if(nameAndAdditional.length() > additionalInf.length())
					name = nameAndAdditional.substring(0, nameAndAdditional.length() - additionalInf.length());
				else
					name = "";
				deal.setName(name);
				String imageUrl = ""+trs.get(1).select("img").attr("src");
				deal.setImageUrl(imageUrl);	
				deals.add(deal);
			}
			
			Elements priceInfo = doc.select("td[align=center][valign=center][width=50%]");
			for(int i = 0; i<deals.size(); i++){
				deals.get(i).setPriceInfo(priceInfo .get(i*2).text());
				deals.get(i).setPrice(priceInfo .get(i*2+1).text());
			}
			
			
			JSONArray arr = JSONArray.fromObject(deals);
			System.out.print(arr+"\n");
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}