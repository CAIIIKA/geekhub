<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script>
			$(document).ready(function(){
				$("#form").validate({
					rules:{
						name: 'required',
						email:{
							email: true
						}
					},
				    messages: {
				    	name: "Please specify your name",
				    	email: {
				    		required: "We need your email address to contact you",
				    		email: "Your email address must be in the format of name@domain.com"
				   		}
					}
				});
				//$("#form").validate();
			});
		</script>
		<style>
			.error{
				color:red;
			}
		</style>
	</head>
	<body>
		<form id="form" action="/saveUser.html">
		    Email <input type="text" value="${user.login}" name="email"><br>
		    Password <input type="text" value="${user.password}" name="password"><br>
		    Name <input type="text" value="${user.name}" name="name"><br>
		    <input type="hidden" name="id" value="${user.id}">
		    <input type="submit" value="Save">
		</form>
		<form action="https://www.google.com.ua/search">
			<input name="q">
			<input type="submit">
		</form>
	</body>	
</html>