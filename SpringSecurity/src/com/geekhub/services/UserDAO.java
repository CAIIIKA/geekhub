package com.geekhub.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;

import com.geekhub.beans.User;

public interface UserDAO {
	
	public List<User> getUsers();
	
	public User getUser(Integer id);
	
	public void saveUser(User user);
	
	public void deleteUser(Integer id);

	public User getUserByUsername(String username);

}
