package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;
import com.geekhub.services.UserDAO;

@Controller
public class BaseController {
	
	@Autowired BaseDao dao;
	@Autowired UserDAO userDao;
	
	@RequestMapping(value="index.html")
	public String index(ModelMap map) {
		map.put("users", userDao.getUsers().size());
//		map.put("tickets", dao.list(Ticket.class).size());
		return "index";
	}
	
	@RequestMapping(value="login.action")
	public String login (ModelMap map){
		return "login";
	}
}
