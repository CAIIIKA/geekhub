package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class UserController {
	
	@Autowired UserDAO userDao;
	
	@RequestMapping(value="listUsers.html")
	public String list(ModelMap map) {
		map.put("users", userDao.getUsers());
		return "users";
	}
	
	@RequestMapping(value="loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		User user = id == null ? new User() : userDao.getUser(id);
		map.put("user", user);
		return "user";
	}
	
	@RequestMapping(value="deleteUser.html")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		userDao.deleteUser(id);
		return "redirect:listUsers.html";
	}
	
	@RequestMapping(value="saveUser.html")
	public String save(User user) {
		userDao.saveUser(user);
		return "redirect:listUsers.html";
	}
}
