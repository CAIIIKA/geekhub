package ua.ck.geekhub.dz1;

public class dz1 {

	static String changeFirstLetter(String str) {
		String str1 = str.substring(0, 1);
		if (str1.toUpperCase().equals(str1))
			return str1.toLowerCase() + str.substring(1);
		else
			return str1.toUpperCase() + str.substring(1);
	}

	public static void main(String[] args) {
		System.out.print(changeFirstLetter(args[0]));
	}
}
