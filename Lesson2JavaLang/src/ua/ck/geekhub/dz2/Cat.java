package ua.ck.geekhub.dz2;

import java.util.Arrays;

public class Cat {

	private int[] rgbColor = new int[3];
	private int age;

	public Cat(int[] rgbColor, int age) {
		this.rgbColor = rgbColor;
		this.age = age;
	}

	public Cat(int red, int green, int blue, int age) {
		rgbColor[0] = red;
		rgbColor[1] = green;
		rgbColor[2] = blue;
		this.age = age;
	}

	public int[] getRgb() {
		return rgbColor;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "Red component = " + rgbColor[0] + "\nGreen component = "
				+ rgbColor[1] + "\nBlue component = " + rgbColor[2]
				+ "\nAge = " + age;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		for (int i = 0; i < 3; i++) {
			hash = hash * 11 + rgbColor[i];
		}
		hash = hash * 11 + age;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !(obj instanceof Cat)) {
			return false;
		}
		Cat obj1 = (Cat) obj;
		int[] rgbColor = obj1.getRgb();
		if (obj1.getAge() != age)
			return false;
		if (!Arrays.equals(rgbColor, this.rgbColor))
			return false;
		return true;
	}
}
