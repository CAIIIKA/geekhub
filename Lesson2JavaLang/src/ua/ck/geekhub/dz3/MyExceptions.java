package ua.ck.geekhub.dz3;

public class MyExceptions {
	
	static void test1() throws Exception1 {
		System.err.println("My Exception");
		throw new Exception1();
	}
	
	static void test2() {
		System.err.println("My RuntimeException");
		throw new RuntimeException1();
	}
	
	public static void main(String [] args) throws Exception1{
		test2();
		test1();
	}

}
