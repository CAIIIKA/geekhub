package ua.ck.geekhub.CharSequence;

public class CharSequence {

	public static void main(String[] args) {
		charSequence(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
	}

	static void charSequence(int m, int n) {
		for (int i = m; i <= n; i++) {
			System.out.print((char) i + " ");
		}
	}
}
