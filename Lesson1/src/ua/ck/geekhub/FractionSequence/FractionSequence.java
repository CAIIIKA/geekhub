package ua.ck.geekhub.FractionSequence;

public class FractionSequence {

	public static void main(String[] args) {
		int input = Integer.parseInt(args[0]);
		fractionSequence(input);
	}

	static void fractionSequence(int n) {

		if (n < 1)
			System.err.println("This myst be a nuber >= 1");

		for (int i = 1; i <= n; i++) {
			System.out.print((float) 1 / i + " ");
		}
	}
}
