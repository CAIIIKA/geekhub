package ua.ck.geekhub.FibonacciSequence;

public class FibonacciSequence {

	public static void main(String[] args) {
		int input = Integer.parseInt(args[0]);
		fibonacci(input);
	}

	static void fibonacci(int n) {
		int fibNumber1 = 1;
		int fibNumber2 = 2;

		if (n < 1)
			System.err.println("This myst be a nuber >= 1");

		System.out.print(fibNumber1 + " ");

		if (n > 1)
			System.out.print(fibNumber2 + " ");

		for (int i = 3; i <= n; i++) {
			int fibNumber3 = fibNumber2 + fibNumber1;
			fibNumber1 = fibNumber2;
			fibNumber2 = fibNumber3;
			System.out.print(fibNumber3 + " ");
		}
	}
}
