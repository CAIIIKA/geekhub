package ua.ck.geekhub.Sum;

public class Sum {

	public static void main(String[] args) {
		float num1 = CheckSystem(args[0]);
		float num2 = CheckSystem(args[1]);
		sum(num1, num2);
	}

	static int degree(int a, int b) {
		int d = 1;
		for (int i = 1; i <= b; i++) {
			d = d * a;
		}
		return d;
	}

	static float CheckSystem(String num) {
		if (num.charAt(0) == '0' && num.charAt(1) == 'x')
			return toDec(num.substring(2, num.length()), 16);
		else if (num.charAt(0) == '0')
			return toDec(num.substring(1, num.length()), 8);
		else
			return Float.parseFloat(num);
	}

	static float toDec(String num, int system) {
		int indexOfpoint = num.indexOf('.');

		if (indexOfpoint == -1)
			return (float) Integer.parseInt(num, system);
		else {
			String num1 = num.substring(0, indexOfpoint)
					+ num.substring(indexOfpoint + 1, num.length());
			int a = degree(system, (num.substring(indexOfpoint + 1)).length());
			return ((float) Integer.parseInt(num1, system)) / a;
		}
	}

	static void sum(float n, float m) {
		System.out.print(n + m);
	}

}
