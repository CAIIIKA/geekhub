<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript"
	src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				title : {
					required : true,
					minlength : 4,
				},
				description : 'required',
			},
			messages : {
				title : {
					required : "Enter title",
					minlength : "Title must be 4 or more letters"
				},
				description : {
					required : "Enter description"
				}
			}
		});
	});
</script>

<style>
.error {
	color: red;
}
</style>

<form action="./saveTicket.html" id=form>
	Title <input type="text" value="${ticket.title}" name="title"><br>
	Description <input type="text" value="${ticket.description}"
		name="description"><br> Status <select name="status">
		<option value="ACTIVE">Active</option>
		<option value="RESOLVED">Resolved</option>
		<option value="TESTED">Tested</option>
		<option value="PUSHED">Pushed</option>
	</select><br> Priority <select name="priority">
		<option value="LOW">Low</option>
		<option value="NORMAL">Normal</option>
		<option value="HIGH">High</option>
	</select><br> Owner <select name="owner.id">
		<c:forEach items="${users}" var="user">
			<option value="${user.id}">${user.name} (${user.id})</option>
		</c:forEach>
	</select><br> <input type="hidden" name="id" value="${ticket.id}">
	<input type="submit" value="Save">
</form>