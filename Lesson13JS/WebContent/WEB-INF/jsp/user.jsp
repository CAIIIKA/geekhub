<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript"
	src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#form").validate({
			rules : {
				login : 'required',
				password : {
					required : true,
					minlength : 4,
					maxlength : 16,

				},
				name : {
					required : true,
					minlength : 3,
				},
			},

			messages : {
				login : {
					required : "Enter login"
				},
				password : {
					minlength : "Password must be 4-16 symbols",
					maxlength : "Password must be 4-16 symbols",
					required : "Please enter your password"
				},
				name : {
					required : "Enter your name",
					minlength : "Name must be 3 or more letters"
				}			
			}
		});
	});
</script>

<style>
.error {
	color: red;
}
</style>

<form action="./saveUser.html"  id="form" >
	Login <input type="text" value="${user.login}" name="login"><br>
	Password <input type="password" value="${user.password}"
		name="password"><br> Name <input type="text"
		value="${user.name}" name="name"><br> <input
		type="hidden" name="id" value="${user.id}"> <input
		type="submit" value="Save">
</form>