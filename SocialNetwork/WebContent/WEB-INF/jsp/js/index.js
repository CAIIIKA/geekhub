$(document).ready(function(){
		if ("${sessionScope.showRecommended}"==1){
			$("#overlay_form").fadeIn(1000);
			positionPopup();
		}
		$("#recFrLink").click(function(){  
			  $("#overlay_form").fadeIn(1000);
			  positionPopup();
		});
		$.ajax({
			url : "./getRecomendedFriends.html",
			type : "GET",
			dataType : "text",
			error : function() {
				alert(error);
			},
			success : function(response) {
				var obj = jQuery.parseJSON(response);
				$.each(obj, function(i) {
					$("#recUsersTemplate").tmpl(obj[i]).appendTo("#recFriends");		
				});
			}
		});	
	$("#close").click(function(){
		$("#overlay_form").fadeOut(500);
		
});
});

//position the popup at the center of the page
function positionPopup(){
if(!$("#overlay_form").is(':visible')){
	return;
} 
$("#overlay_form").css({
	left: ($(window).width() - $('#overlay_form').width()) / 2,
	top: ($(window).width() - $('#overlay_form').width()) / 7,
	position:'absolute'
});
}

//maintain the popup at center of the page when browser resized
$(window).bind('resize',positionPopup);

function addPost() {
	var userId = "${getedUser.id}";
	var message = document.getElementById('wallMessage').value;

	$.ajax({
		url : "./addWallMessage.action?receiverId=" + userId + "&message="
				+ message,
		type : "GET",
		dataType : "text",

		success : function(response) {
			var obj = jQuery.parseJSON(response);
			$("#wallMessageTemplate").tmpl(obj).prependTo("#newPost");
			$(".wallMessage").val("");
		}
	});

}

function addComment(wallMesageId) {
	var message = document.getElementById(wallMesageId).value;
	$.ajax({
		url : "./addWallMessageComment.action?wallMessageId=" + wallMesageId + "&message="
				+ message,
		type : "GET",
		dataType : "text",

		success : function(response, wallMessageId) {
			
			var obj = jQuery.parseJSON(response);
			$("#CommentTemplate").tmpl(obj).appendTo("#newComment"+wallMesageId);
			$(".wallComment").val("");
		}
	});
}

var formatedDate = function (date) {
	var d = new Date(date.time);
	month = d.getMonth() + 1+"";
	if(month.length==1)
		month = "0" + month;
	currentDate = d.getDate()+"";
	if(currentDate.length==1)
		currentDate = "0" + currentDate;
	year = d.getYear();
	if(year < 1000)
		year = year+1900;
	hh = d.getHours();
	mm = d.getMinutes();
	ss = d.getSeconds();
	return currentDate+"-"+month+"-"+year+" "+hh+":"+mm+":"+ss;
}

