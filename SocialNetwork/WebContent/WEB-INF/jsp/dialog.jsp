<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${getedUser.name} ${getedUser.surname}</title>
<style type="text/css">
	<jsp:includepage="css/styles.css" flush="false"/>
</style>

<style type="text/css">
	<jsp:includepage="css/IndexPopup.css" flush="false"/>
</style>


<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/jquery.tmpl.js"></script>
<script type="text/javascript" src="./js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript"><%@ include file="js/index.js" %></script>

<script>

var auto_refresh = setInterval(
		function ()
		{	
			function sendMessage() {
				var userId = "${getedUser.id}";
				var message = document.getElementById('message').value;

				
				$.ajax({
					url : "./sendMessage.html?receiverId=" + userId + "&message="
							+ message,
					type : "GET",
					dataType : "text",

					success : function(response) {
						var obj = jQuery.parseJSON(response);
						$("#messageTemplate").tmpl(obj).prependTo("#newPost");
						$(".message").val("");
					}
				});

			}
			var userId = "${getedUser.id}";
			$.ajax({
				url : "./getNewMessages.html?id=" + userId,
				type : "GET",
				dataType : "text",
				
				success : function(response) {
					var obj = jQuery.parseJSON(response);
					$.each(obj, function(i) {
						$("#messageTemplate").tmpl(obj[i]).prependTo("#newPost");		
					});
				}
			});		
		},2000);

function sendMessage() {
	var userId = "${getedUser.id}";
	var message = document.getElementById('message').value;

	
	$.ajax({
		url : "./sendMessage.html?receiverId=" + userId + "&message="
				+ message,
		type : "GET",
		dataType : "text",

		success : function(response) {
			var obj = jQuery.parseJSON(response);
			$("#messageTemplate").tmpl(obj).prependTo("#newPost");
			$(".message").val("");
		}
	});

}
</script>

<script id="messageTemplate" type="text/x-jquery-tmpl">
	<img src="./getUsersAvatar.html?id={{html owner.id}}"
				width="35" height="35">
    <a href="./getPage.html?id={{html owner.id}}"> 
		{{html owner.name}} {{html owner.surname}}
	</a>
	<br>
	{{html message}}
	<br>
	����: {{html formatedDate(time)}}
	<br>	
					
</script>

</head>
<body>
	<div id="wrapper">
		<div id="wrap-content">
			<div id="header">
				<ul>
					<li><a href="./index.html">��� ��������</a></li>
					<li><a href="./getFriends.html">��� ������</a></li>
					<li><a href="#" id = "recFrLink" class = "recFrLink">��������������� ������</a></li>
					<form id="overlay_form" style="display:none">
						<a href="#" id="close" >Close</a>
						<h2>��������������� ������</h2>
						<div id = "recFriends" class ="recFriends"></div>
					</form>
					<li><a href="./find.html">����� �����</a></li>
					<li><a href="/">dsf</a></li>
				</ul>
			</div>

			<div id="content">
				<div class="wall">
					<form action="./sendMessage.html" id="form" class="form"
						onsubmit="sendMessage();return false;">
						Message:
						<textarea cols="30" rows="5" name="message" class="message"
							id="message"></textarea>
						<input type="hidden" name="receiverId" value="${getedUser.id}">
						<br /> <input type="submit" name="add" value="��������">
					</form>
					<div class="newPost" id = "newPost"></div>
					<c:forEach items="${messages}" var="message">
						<img src="./getUsersAvatar.html?id=${message.owner.id}"
							width="35" height="35">
						<a href="./getPage.html?id=${message.owner.id}"> <c:out
								value="${message.owner.name}" /> <c:out
								value="${message.owner.surname}: " />
						</a>
						<br>
						<c:out value="${message.message}" />
						<br>
							����:<fmt:formatDate value="${message.time}" type="date"
							pattern="dd-MM-yyyy HH:mm:ss" />
						<br>			
					</c:forEach>
				</div>

			</div>
			<!--end content-->

		</div>
		<!--end wrap-content-->
	</div>
	<!--end wrapper-->


</body>
</html>