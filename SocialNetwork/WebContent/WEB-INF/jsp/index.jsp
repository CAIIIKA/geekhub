<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8">
<title>${getedUser.name} ${getedUser.surname}</title>
<style type="text/css">
	<jsp:includepage="css/styles.css" flush="false"/>
</style>

<style type="text/css">
	<jsp:includepage="css/IndexPopup.css" flush="false"/>
</style>

<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/jquery.tmpl.js"></script>
<script type="text/javascript" src="./js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript"><%@ include file="js/index.js" %></script>

<script>
var auto_refresh = setInterval(
		function ()
		{	
			$.ajax({
				url : "./getNewMessagesCount.html",
				type : "GET",
				dataType : "text",
				
				success : function(response) {
					var obj = jQuery.parseJSON(response);
					$('.messCount').empty();
					$('.messCount').append(obj.count);	
					
				}
			});	
			$.ajax({
				url : "./getFriendsInvitationCount.html",
				type : "GET",
				dataType : "text",
				
				success : function(response) {
					var obj = jQuery.parseJSON(response);
					$('.friendsCount').empty();
					$('.friendsCount').append(obj.count);	
					
				}
			});		
		},2000);
</script>

<script>
	function like(id, item){
		$.ajax({
			url : ".//like.html?id="+id+"&item="+item,
			type : "GET",
			dataType : "text",
			error : function() {
			},
			success : function(response) {
			}
		});
			
		$.post("/like.html?id="+id+"&item="+post);
	}
</script>

<script id="wallMessageTemplate" type="text/x-jquery-tmpl">
	<img src="./getUsersAvatar.html?id={{html owner.id}}"
				width="35" height="35">
    <a href="./getPage.html?id={{html owner.id}}"> 
		{{html owner.name}} {{html owner.surname}}
	</a>
	<br>
	{{html message}}
	<br>
	Дата: {{html formatedDate(time)}}
	<br>
	{{if owner.id == ${loggedUser.id} || receiver.id == ${loggedUser.id}}}
		<a href="./deleteWallMessage.action?id={{html id}}">Удалить</a>
		<br>
	{{/if}}

	<a href="#" onClick="like({{html id}},'post')">Like</a>
	<a href="./getCommentLikes.html?id={{html id}}&item=post">
	Показать лайкеров</a>	

	<div style="margin: 0px 0px 0px 50px;">
	<div class = "newComment{{html id}}" id = "newComment{{html id}}"></div>
		<form action="./addWallMessageComment.action" onsubmit="addComment({{html id}});return false;">
			Комеентарий: <input type="text" size="30" name="message" id = {{html id}} class="wallComment"> <input
				type="hidden" name="wallMessageId" value={{html id}}>
			<br /> <input type="submit" name="add" value="Комменитровать">
		</form>
	</div>
	</div>
					
</script>

<script id="CommentTemplate" type="text/x-jquery-tmpl">
	<img src="./getUsersAvatar.html?id={{html owner.id}}"
				width="35" height="35">
    <a href="./getPage.html?id={{html owner.id}}"> 
		{{html owner.name}} {{html owner.surname}}
	</a>
	<br>
	{{html message}}
	<br>
	Дата: {{html formatedDate(time)}}
	<br>
	{{if owner.id == ${loggedUser.id} || receiver.id == ${loggedUser.id}}}
		<a href="./deleteWallMessageComment.action?id={{html id}}">Удалить комментарий</a>
		<br>
	{{/if}}
	<a href="#" onClick="like({{html id}},'comment')">Like</a>
	<a href="./getCommentLikes.html?id={{html id}}&item=comment">
	Показать лайкеров
	</a>				
</script>

<script id="recUsersTemplate" type="text/x-jquery-tmpl">
		<img src="./getUsersAvatar.html?id={{html id}}"
				width="35" height="35">
		<a href="./getPage.html?id={{html id}}">
			{{html name}} {{html surname}}
		</a>						
</script>


</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.showRecommended==1}">
			<form id="overlay_form" style="display:none">
			<a href="#" id="close" >Close</a>
			<h2>Рекомендованные друзья</h2>
			<div id = "recFriends" class ="recFriends"></div>
			</form>
			<c:set var="showRecommended" value="${0}" scope="session"  />
		</c:when>
	</c:choose>
	<div id="wrapper">
		<div id="wrap-content">
			<div id="header">
				<ul>
					<li><a href="./index.html">Моя Страница</a></li>
					<li><a href="./getFriendsPage.html">Мои Друзья</a></li>
					<li><a href="#" id = "recFrLink" class = "recFrLink">Рекомендованные друзья</a></li>
					<form id="overlay_form" style="display:none">
						<a href="#" id="close" >Close</a>
						<h2>Рекомендованные друзья</h2>
						<div id = "recFriends" class ="recFriends"></div>
					</form>
					<li><a href="./find.html">Поиск Людей</a></li>
					<li><a href="./logout.html">Выход</a></li>
				</ul>
			</div>

			<div id="sidebar">

				<div class="panel-side">
					<div class="user-bg">
						<span>${getedUser.name}
							<c:choose>
								<c:when test="${getedUser.id==sessionScope.loggedUser.id}">
									(Это вы)<br>
									<a href="./uploadAvatarPage.html">Изенить аватар</a>
								</c:when>
							</c:choose>
						</span>
					</div>
					<div class="avatar">
						<img src="./getUsersAvatar.html?id=${getedUser.id}" width="130"
							height="119">
					</div>
					<ul class="check-user">
						<c:choose>
							<c:when test="${getedUser.id!=loggedUser.id}">
								<li><a href="./getDialog.html?id=${getedUser.id}">Написать</a></li>
								<li><a href="./addFriend.html?userId=${getedUser.id}">Добавить в друзья</a></li>
							</c:when>
				
						</c:choose>
						<li><a href="./index.html">Моя страница</a></li>
						<li><a href="./edit.html">Редактировать мою страницу</a></li>
						<li><a href="./getFriendsPage.html">Мои друзья</a>
						<div id = "friendsCount" class = "friendsCount"></div>
						</li>
						<li><a href="./myInboxMessages.html">Мои сообщения</a>
						<div id = "messCount" class = "messCount"></div>
						</li>
						<li><a href="./find.html">Поиск Людей</a></li>
						<li><a href="./logout.html">Выход</a></li>
					</ul>
				</div>
				<!--end panel-side-->

				


			</div>
			<!--end sidebar-->


			<div id="content">
			

				<div class="info">
					<b>${getedUser.name} ${getedUser.surname}
						${getedUser.patronymic}</b><br> День рождения:
					<fmt:formatDate value="${getedUser.date}" type="date"
						pattern="dd MMMM yyyy года" />
					<br> Страна: ${getedUser.country.nameCountry}<br> Регион:
					${getedUser.region.nameRegion}<br> Город:
					${getedUser.city.nameCity}<br> Адрес: ${getedUser.adress}<br>
				</div>

				<div class="wall">
					<form action="./addWallMessage.action" id="form" class="form"
						onsubmit="addPost();return false;">
						Message:
						<textarea cols="30" rows="5" name="message" class="wallMessage"
							id="wallMessage"></textarea>
						<input type="hidden" name="receiverId" value="${getedUser.id}">
						<br /> <input type="submit" name="add" value="Написать">
					</form>
					<div class="newPost" id = "newPost"></div>
					<c:forEach items="${wallMessages}" var="wallMessage">
						<img src="./getUsersAvatar.html?id=${wallMessage.owner.id}"
							width="35" height="35">
						<a href="./getPage.html?id=${wallMessage.owner.id}"> <c:out
								value="${wallMessage.owner.name}" /> <c:out
								value="${wallMessage.owner.surname}: " />
						</a>
						<br>
						<c:out value="${wallMessage.message}" />
						<br>
							Дата:<fmt:formatDate value="${wallMessage.time}" type="date"
							pattern="dd-MM-yyyy HH:mm:ss" />
						<br>
						<c:choose>
							<c:when
								test="${wallMessage.owner.id==loggedUser.id||wallMessage.receiver.id==loggedUser.id}">
								<a href="./deleteWallMessage.action?id=${wallMessage.id}">Удалить</a>
								<br>
							</c:when>
						</c:choose>
						<a href="#" onClick="like(${wallMessage.id},'post')">Like</a>
						<a href="./getCommentLikes.html?id=${wallMessage.id}&item=post">Показать лайкеров</a>
						<div style="margin: 0px 0px 0px 50px;"> 
							<c:forEach items="${wallMessage.comments}"
								var="wallMessageComment">
								<img
									src="./getUsersAvatar.html?id=${wallMessageComment.owner.id}"
									width="35" height="35">
								<a href="./getPage.html?id=${wallMessageComment.owner.id}">
									<c:out value="${wallMessageComment.owner.name}" /> <c:out
										value="${wallMessageComment.owner.surname}: " />
								</a>
								<br>
								<c:out value="${wallMessageComment.message}" />
								<br> Дата:
								<fmt:formatDate value="${wallMessageComment.time}" type="date"
									pattern="dd-MM-yyyy hh:mm:ss" />
								<br>
								<c:choose>
									<c:when
										test="${wallMessageComment.owner.id==loggedUser.id||wallMessageComment.wallMessage.receiver.id==loggedUser.id}">
										<a
											href="./deleteWallMessageComment.action?id=${wallMessageComment.id}">Удалить комментарий</a>
										<br>
									</c:when>
								</c:choose>
								<a href="#" onClick="like(${wallMessageComment.id},'comment')">Like</a>
								<a href="./getCommentLikes.html?id=${wallMessageComment.id}&item=comment">
								Показать лайкеров
								</a>
							</c:forEach>
							<div class = "newComment${wallMessage.id}" id = "newComment${wallMessage.id}"></div>
							<form action="./addWallMessageComment.action" onsubmit="addComment('${wallMessage.id}');return false;">
								Комеентарий: <input type="text" size="30" name="message" id = '${wallMessage.id}' class="wallComment"> <input
									type="hidden" name="wallMessageId" value="${wallMessage.id}">
								<br /> <input type="submit" name="add" value="Комменитровать">
							</form>
						</div>
					</c:forEach>
				</div>

			</div>
			<!--end content-->

		</div>
		<!--end wrap-content-->
	</div>
	<!--end wrapper-->


</body>
</html>