<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>��� ������</title>
<style type="text/css">
	<jsp:includepage="css/styles.css" flush="false"/>
</style>
<style type="text/css"><jsp:includepage="css/IndexPopup.css" flush="false"/>
</style>

<style type="text/css">
	<jsp:includepage="css/LoginStyle.css" flush="false"/>
</style>


<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/jquery.tmpl.js"></script>
<script type="text/javascript" src="./js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript"><%@ include file="js/locationForSearch.js" %></script>
<script language="javascript" type="text/javascript"><%@ include file="js/index.js" %></script>

<script>
	function invitations(){
		$(".container").empty();
		$.ajax({
			url: "./getInInvitations.html",
			type: "GET",
			dataType: "text",
			success : function(response) {
				var obj = jQuery.parseJSON(response);
				$.each(obj, function(i) {
					$("#invitationTemplate").tmpl(obj[i]).appendTo("#container");		
				});
			}		
		});
	}
</script>

<script>
	function friends(){
		$(".container").empty();
		$.ajax({
			url: "./getFriends.html",
			type: "GET",
			dataType: "text",
			success : function(response) {
				var obj = jQuery.parseJSON(response);
				$.each(obj, function(i) {
					$(".container").empty();
					$("#friendsTemplate").tmpl(obj[i]).appendTo("#container");		
				});
			}		
		});
	}
</script>

<script>
	function outInvitation(){
		$(".container").empty();
		$.ajax({
			url: "./getOutInvitations.html",
			type: "GET",
			dataType: "text",
			success : function(response) {
				var obj = jQuery.parseJSON(response);
				$.each(obj, function(i) {
					$(".container").empty();
					$("#outInvitationTemplate").tmpl(obj[i]).appendTo("#container");		
				});
			}		
		});
	}
</script>

<script>
	$(document).ready(function(){
		friends();
	});
</script>

<script id="invitationTemplate" type="text/x-jquery-tmpl">
		<img src="./getUsersAvatar.html?id={{html id}}"
				width="35" height="35">
		<a href="./getPage.html?id={{html id}}">
			{{html name}} {{html surname}}
		</a>
		<br>
		<a href="./addFriend.html?userId={{html id}}">�������� � ������</a>						
</script>

<script id="outInvitationTemplate" type="text/x-jquery-tmpl">
		<img src="./getUsersAvatar.html?id={{html id}}"
				width="35" height="35">
		<a href="./getPage.html?id={{html id}}">
			{{html name}} {{html surname}}
		</a>
		<br>
		<a href="./cancleInvitations.html?userId={{html id}}">�������� ������</a>						
</script>

<script id="friendsTemplate" type="text/x-jquery-tmpl">
		<img src="./getUsersAvatar.html?id={{html id}}"
				width="35" height="35">
		<a href="./getPage.html?id={{html id}}">
			{{html name}} {{html surname}}
		</a>
		<br>
		<a href="./deleteFriend.html?userId={{html id}}"> ������� � ������</a>
		<br>						
</script>

</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.showRecommended==1}">
			<form id="overlay_form" style="display: none">
				<a href="#" id="close">Close</a>
				<h2>��������������� ������</h2>
				<div id="recFriends" class="recFriends"></div>
			</form>
			<c:set var="showRecommended" value="${0}" scope="session" />
		</c:when>
	</c:choose>
	<div id="wrapper">
		<div id="wrap-content">
			<div id="header">
				<ul>
					<li><a href="./index.html">��� ��������</a></li>
					<li><a href="./getFriends.html">��� ������</a></li>
					<li><a href="./find.hrml">�����</a></li>
					<li><a href="./logout.html">�����</a></li>
				</ul>
			</div>


			<div id="content">
				<a href="#" onClick="invitations()">�������� ������</a>
				<a href="#" onClick="friends()">��� ������</a>
				<a href="#" onClick="outInvitation()">��������� ������</a>
				<br>
				<div id = "container" class = "container">
				</div>		
			</div>
			<!--end content-->

		</div>
		<!--end wrap-content-->
	
	</div>
	<!--end wrapper-->


</body>
</html>