<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>�������������</title>
<style type="text/css">
	<jsp:includepage="css/styles.css" flush="false"/>
</style>
<style type="text/css"><jsp:includepage="css/IndexPopup.css" flush="false"/>
</style>

<style type="text/css">
	<jsp:includepage="css/LoginStyle.css" flush="false"/>
</style>

<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/jquery.tmpl.js"></script>
<script type="text/javascript" src="./js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript"><%@ include file="js/locationForSearch.js" %></script>
<script language="javascript" type="text/javascript"><%@ include file="js/index.js" %></script>

<script>
function updateSearch() {
		var name = document.getElementById('name').value;
		var surname = document.getElementById('surname').value;
		var patronymic = document.getElementById('patronymic').value;
		var countryId = document.getElementById('countryId').value;
		var regionId = document.getElementById('regionId').value;
		var cityId = document.getElementById('cityId').value;
		$.ajax({
			url : ".//getFinded.html?name=" + name + "&surname=" + surname
			+ "&patronymic=" + patronymic +"&countryId=" + countryId +
			"&regionId="+ regionId + "&cityId=" + cityId,
			type : "GET",
			dataType : "text",

			success : function(response) {
				var obj = jQuery.parseJSON(response);
				$(".finded").empty();
				$.each(obj, function(i) {
					$("#findedTemplate").tmpl(obj[i]).appendTo("#finded");
					
				});
			}
		});
	}
</script>
<script>
$(document).ready(function(){
	$("#name").keyup(function() {
	  updateSearch();
	});
	
	$("#surname").keyup(function() {
		  updateSearch();
	});
	
	$("#patronymic").keyup(function() {
		  updateSearch();
	});

});
</script>

<script id="findedTemplate" type="text/x-jquery-tmpl">
		<img src="./getUsersAvatar.html?id={{html id}}"
				width="35" height="35">
		<a href="./getPage.html?id={{html id}}">
			{{html name}} {{html surname}}
		</a>
		<br>						
</script>

</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.showRecommended==1}">
			<form id="overlay_form" style="display: none">
				<a href="#" id="close">Close</a>
				<h2>��������������� ������</h2>
				<div id="recFriends" class="recFriends"></div>
			</form>
			<c:set var="showRecommended" value="${0}" scope="session" />
		</c:when>
	</c:choose>
	<div id="wrapper">
		<div id="wrap-content">
			<div id="header">
				<ul>
					<li><a href="./index.html">��� ��������</a></li>
					<li><a href="./getFriends.html">��� ������</a></li>
					<li><a href="./find.hrml">�����</a></li>
					<li><a href="./logout.html">�����</a></li>
				</ul>
			</div>


			<div id="content">
				<section class="container">
				<form action="./edit.html">
		<table>
			<tr>
				<td>Password:</td>
				<td><input type="password" size="30" value="${user.password}"
					name="password"></td>
			</tr>
			<tr>
				<td>e-mail:</td>
				<td><input type="text" size="30" value="${user.email}"
					name="email"></td>
			</tr>
			<tr>
				<td>Name</td>
				<td><input type="text" size="30" value="${user.name}"
					name="name"></td>
			</tr>
			<tr>
				<td>Surname</td>
				<td><input type="text" value="${user.surname}" name="surname"></td>
			</tr>
			<tr>
				<td>Patronymic</td>
				<td><input type="text" value="${user.patronymic}"
					name="patronymic"></td>
			</tr>
		</table>
		<br>
		<div>
			Country <select name="country.id" onchange="javascript: getRegions()"
				id="countryId" class="countries">
				<option value="">-- please select country --</option>
				<c:forEach items="${countries}" var="country">
					<c:choose>
						<c:when test="${country.id==user.country.id}">
							<option value="${user.country.id}" selected>${user.country.nameCountry}</option>
						</c:when>
						<c:otherwise>
							<option value="${country.id}">${country.nameCountry}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select> <br>
		</div>
		<div class="reg">
			Region <select name="region.id" class="regions"
				onchange="getCities()" id="regionId">
				<option value="">-- please select region --</option>
				<c:forEach items="${regions}" var="region">
					<c:choose>
						<c:when test="${region.id==user.region.id}">
							<option value="${user.region.id}" selected>${user.region.nameRegion}</option>
						</c:when>
						<c:otherwise>
							<option value="${region.id}">${region.nameRegion}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select> <br>
		</div>
		<div class="cit">
			City <select name="city.id" class="cities" id="cityId">
				<option value="">-- please select city --</option>
				<c:forEach items="${cities}" var="city">
					<c:choose>
						<c:when test="${city.id==user.city.id}">
							<option value="${user.city.id}" selected>${user.city.nameCity}</option>
						</c:when>
						<c:otherwise>
							<option value="${city.id}">${city.nameCity}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</div>
		<br> Adress <input type="text" value="${user.adress}"
			name="adress"><br> Date: <input type="text"
			id="datepicker" name="date"
			value="<fmt:formatDate value="${user.date}" type="date"
			pattern="MM/dd/yyyy" />" readonly='true'/>
		<br> <input type="submit" name="reg" value="Registration"
			onClick="clearNullSelect()">

	</form>
				<br>
				<c:forEach items="${users}" var="user">
					<a href="./getPage.html?id=${user.id}"> <c:out
							value="${user.name}" /> <c:out value="${user.surname}: " />
					</a>
					<br>
				</c:forEach>

				<div id="finded" class="finded"></div>
			</div>
			<!--end content-->

		</div>
		<!--end wrap-content-->
	</div>
	<!--end wrapper-->


</body>
</html>