<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="./js/jquery.js"></script>
<script type="text/javascript" src="./js/jquery.tmpl.js"></script>
<script type="text/javascript" src="./js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript"><%@ include file="js/location.js" %></script>

<title>Recommended friends</title>
</head>

<body>
	<c:forEach items="${friends}" var="friend">
		<a href="./getPage.html?id=${friend.id}" target="_blank"> <c:out
				value="${friend.name}" /> <c:out value="${friend.surname}: " />
		</a>
		<br>
	</c:forEach>
</body>
</html>