<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="http://code.jquery.com/jquery-latest.js"></script>


<form action="./saveUser.html">
	Name <input type="text" value="${user.name}" name="name"><br>
	Surname <input type="text" value="${user.surname}" name="surname"><br> 
	Patronymic <input type="text" value="${user.patronymic}" name="patronymic"><br>
	<br> Country <select name="country.id">
		<c:forEach items="${countries}" var="country">
			<c:choose>
            	<c:when test="${country.id==user.country.id}">
                	<option value="${country.id}" selected>${country.nameCountry}</option>
                </c:when>               
              </c:choose>
			<option value="${country.id}">${country.nameCountry}</option>
		</c:forEach>
	</select><br>
	City <input type="text" value="${user.city}" name="city"><br>
	Adress <input type="text" value="${user.adress}" name="adress"><br>
	E-mail <input type="text" value="${user.email}" name="email"><br>
	<input type="hidden" name="login" value="${user.login}">
	<input type="hidden" name="password" value="${user.password}">
	<input type="hidden" name="id" value="${user.id}">
	<input type="submit" value="Save">
</form>