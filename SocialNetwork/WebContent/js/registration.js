function next() {
	$(".mainInfo").prop("hidden", true);
	$(".next").prop("hidden", true);
	$(".back").prop("hidden", false);
	$(".additionalInformation").prop("hidden", false);
}

function back() {
	$(".mainInfo").prop("hidden", false);
	$(".next").prop("hidden", false);
	$(".back").prop("hidden", true);
	$(".additionalInformation").prop("hidden", true);
}

$(function() {
	$("#datepicker").datepicker({
		changeMonth : true,
		changeYear : true,
		minDate : "01/01/1910",
		maxDate : "12/31/2005",
		yearRange : '-100:+10'

	});
	$("#datepicker").datepicker($.datepicker.regional["ru"]);
});
