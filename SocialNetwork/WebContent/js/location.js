function getRegions() {
	var counId = document.getElementById('countryId').value;

	$.ajax({
		url : "./getRegions.action?countryId=" + counId,
		type : "GET",
		dataType : "text",
		error : function() {
			clearCitiesAndRegions();
		},
		success : function(response) {
			$(".regions").empty();
			$(".cities").empty();
			var obj = jQuery.parseJSON(response);
			$('.regions').append(
					'<option value="">�������� ������</option>');

			$.each(obj, function(i) {
				$('.regions').append(
						'<option value=' + obj[i].id + '>' + obj[i].nameRegion
								+ '</option>');
			});

			$(".reg").prop("hidden", false);
			$(".cit").prop("hidden", true);

		}
	});
}
function clearCitiesAndRegions() {
	$(".regions").empty();
	$(".cities").empty();
	$(".reg").prop("hidden", true);
	$(".cit").prop("hidden", true);
}

function getCities() {
	var regionId = document.getElementById('regionId').value;
	$.ajax({
		url : "./getCities.action?regionId=" + regionId,
		type : "GET",
		dataType : "text",
		error : function() {
			$(".cities").empty();
			$(".cit").prop("hidden", true);
		},
		success : function(response) {
			$(".cities").empty();
			$('.cities').append(
					'<option value="">�������� �����</option>');
			var obj = jQuery.parseJSON(response);
			$.each(obj, function(i) {
				$('.cities').append(
						'<option value=' + obj[i].id + '>' + obj[i].nameCity
								+ '</option>');
			});
			$(".cit").prop("hidden", false);
		}
	});
}
function clearNullSelect() {
	var counId = document.getElementById('countryId').value;
	if (counId == "") {
		$(".countries").empty();
	}
	var regionId = document.getElementById('regionId').value;
	if (regionId == "") {
		$(".regions").empty();
	}
	var cityId = document.getElementById('cityId').value;
	if (cityId == "") {
		$(".cities").empty();
	}

}