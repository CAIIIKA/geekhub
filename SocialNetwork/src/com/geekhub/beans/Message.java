package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="MESSAGE")
public class Message {
	
	@GeneratedValue
	@Id
	@Column(name="MESSAGE_ID")
	private Integer id;

	@Column(name="MESSAGE")
    private String message;
	
	@Column(name="TIME")
    private Date time = new Date();
	
	@Column(name="STATUS")
    private boolean status;

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date date) {
		this.time = date;
	}

	@ManyToOne
	@JoinColumn(name="OWNER_ID")
	private User owner;
	
	@ManyToOne
	@JoinColumn(name="RECEIVER_ID")
	private User receiver;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}
	
}
