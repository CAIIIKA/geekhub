package com.geekhub.beans;

import javax.persistence.*;

@Entity
@Table(name = "country_")
public class Country {
	@GeneratedValue
	@Id
	@Column(name="id_country")
	private Integer id;
	
	@Column(name="country_name_en")
    private String nameCountry;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameCountry() {
		return nameCountry;
	}

	public void setNameCountry(String nameCountry) {
		this.nameCountry = nameCountry;
	}

}
