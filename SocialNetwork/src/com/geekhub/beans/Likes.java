package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name="Likes" )
public class Likes {
	
	@GeneratedValue
	@Id
	@Column(name="LIKE_ID")
	private Integer likeId;
	
	@ManyToOne
	@JoinColumn(name="user")
	@Index(name="user_Index")
	private User user;
	
	@Column(name="Item")
	private String item;
	
	public Integer getLikeId() {
		return likeId;
	}

	public void setLikeId(Integer likeId) {
		this.likeId = likeId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	@Column(name="Item_ID")
	private Integer itemId;
	
}
