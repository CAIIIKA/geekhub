package com.geekhub.beans;

import javax.persistence.*;

@Entity
@Table(name = "city_")
public class City {
	@GeneratedValue
	@Id
	@Column(name="id_city")
	private Integer id;
	
	@Column(name="city_name_en")
    private String nameCity;
	
	@ManyToOne
	@JoinColumn(name = "id_region")
	private Region region;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameCity() {
		return nameCity;
	}

	public void setNameCity(String nameCity) {
		this.nameCity = nameCity;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}



}
