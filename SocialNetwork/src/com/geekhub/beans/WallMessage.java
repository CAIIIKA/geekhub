package com.geekhub.beans;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="WALLMESSAGE")
public class WallMessage {
	
	@GeneratedValue
	@Id
	@Column(name="WALL_MESSAGE_ID")
	private Integer id;

	@Column(name="MESSAGE")
    private String message;
	
	@ManyToOne
	@JoinColumn(name="OWNER_ID")
	private User owner;
	
	@ManyToOne
	@JoinColumn(name="RECEIVER_ID")
	private User receiver;
	
	@Column(name="TIME")
    private Date time = new Date();
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="wallMessage")
	    private Collection<WallMessageComment> comments;

	public Collection<WallMessageComment> getComments() {
		return comments;
	}

	public void setComments(Collection<WallMessageComment> comments) {
		this.comments = comments;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
