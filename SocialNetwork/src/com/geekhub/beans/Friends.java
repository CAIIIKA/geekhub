package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name="FRIENDS" )
public class Friends {
	
	@GeneratedValue
	@Id
	@Column(name="FRIENDS_ID")
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="user_1_ID")
	@Index(name="user_1_Index")
	private User user_1;
	
	@ManyToOne
	@JoinColumn(name="user_2_ID")
	@Index(name="user_2_Index")
	private User user_2;
	

	@Column(name="STATUS")
	private String status;
	
	@Column(name="noFriendField")
	private boolean noFriendField;
	
	public boolean isNoFriendField() {
		return noFriendField;
	}



	public void setNoFriendField(boolean noFriendField) {
		this.noFriendField = noFriendField;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser_1() {
		return user_1;
	}



	public void setUser_1(User user_1) {
		this.user_1 = user_1;
	}



	public User getUser_2() {
		return user_2;
	}



	public void setUser_2(User user_2) {
		this.user_2 = user_2;
	}



	public String isStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}		
}
