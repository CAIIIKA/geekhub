package com.geekhub.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="WALLMESSAGECOMMENTS")
public class WallMessageComment {
	
	@GeneratedValue
	@Id
	@Column(name="WALLMESSAGECOMMENTS_ID")
	private Integer id;

	@Column(name="MESSAGE")
    private String message;
	
	
	@ManyToOne
	@JoinColumn(name="WALL_MESSAGE_ID")
	private WallMessage wallMessage;
	
	@ManyToOne
	@JoinColumn(name="ID")
	private User owner;
	
	@Column(name="TIME")
    private Date time = new Date();
	
	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}


	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public WallMessage getWallMessage() {
		return wallMessage;
	}

	public void setWallMessage(WallMessage wallMessage) {
		this.wallMessage = wallMessage;
	}

	
}
