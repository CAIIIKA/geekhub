package com.geekhub.beans;



import java.util.Collection;
import java.util.Date;
import java.util.HashSet;


import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;



@Entity
@Table(name="USER")
public class User implements UserDetails{
	
	private static final long serialVersionUID = 1L;

	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;

	@Column(name="LOGIN")
    private String login;
	
	@Column(name="PASSWORD")
    private String password;

	@Column(name="NAME")
    private String name;
	
	@Column(name="SURNAME")
    private String surname;
	
	@Column(name="PATRONYMIC")
    private String patronymic;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY")
    private Country country;
	
	@ManyToOne
	@JoinColumn(name="REGION")
    private Region region;
	
	@ManyToOne
	@JoinColumn(name="CITY")
    private City city;
	
	@Column(name="BIRTHDAY")
    private Date date = new Date();

	@Column(name="ADRESS")
    private String adress;
	
	@Column(name="EMAIL")
    private String email;
	
	@Column(name = "ROLES")
	private String roles;
	
	@Column(name = "AVATAR")
	private byte[] avatar;
	
	public Date getDate() {
		
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] file) {
		this.avatar = file;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (final String role : roles.split(",")) {
			if (role != null && !"".equals(role.trim())) {
				GrantedAuthority grandAuthority = new GrantedAuthority() {
					private static final long serialVersionUID = 3958183417696804555L;

					public String getAuthority() {
						System.out.print(role.trim());
						return role.trim();
					}
				};
				authorities.add(grandAuthority);
			}
		}
		System.out.print(authorities);
		return authorities;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
}

