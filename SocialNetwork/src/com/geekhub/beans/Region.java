package com.geekhub.beans;

import javax.persistence.*;

@Entity
@Table(name = "region_")
public class Region {
	@GeneratedValue
	@Id
	@Column(name="id_region")
	private Integer id;
	
	@Column(name="region_name_en")
    private String nameRegion;
	
	@ManyToOne
	@JoinColumn(name = "id_country")
	private Country country;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameRegion() {
		return nameRegion;
	}

	public void setNameRegion(String nameRegion) {
		this.nameRegion = nameRegion;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}


	
}
