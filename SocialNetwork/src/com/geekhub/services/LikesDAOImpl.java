package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Likes;
import com.geekhub.beans.User;

@Repository
@Transactional
public class LikesDAOImpl implements LikesDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	WallMessageDAO wallMessageDAO;

	@Override
	public void like(Integer id, User user, String item) {
		Likes like = (Likes) sessionFactory
		.getCurrentSession()
		.createQuery(
				"from Likes where user = ? AND Item_ID = ? AND Item = ?" )
		.setInteger(0, user.getId()).setInteger(1, id).setString(2, item).uniqueResult();
		if (like==null){
			Likes newLike = new Likes();
			newLike.setUser(user);
			newLike.setItem(item);
			newLike.setItemId(id);
			sessionFactory.getCurrentSession().save(newLike);
		}
		else {
			sessionFactory.getCurrentSession().delete(like);
		}
			
			
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Likes> getLikes(Integer id, String item) {
		return  sessionFactory
		.getCurrentSession()
		.createQuery(
				"from Likes where Item_ID = ? AND Item = ?")
		.setInteger(0, id).setString(1, item).list();
	}

	

}