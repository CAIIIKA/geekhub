package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.*;

@Repository
@Transactional
public class CountryDAOImpl implements CountryDAO{
	
	@Autowired SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	
	@Override
	public List<Country> getListCountries() {
		return sessionFactory.getCurrentSession().createQuery("from Country").list();
	}
	@Override
	public Country getCountry(Integer id) {
		return (Country) sessionFactory.getCurrentSession().get(Country.class, id);
	}

}
