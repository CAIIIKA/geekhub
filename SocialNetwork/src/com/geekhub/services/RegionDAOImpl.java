package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Region;


@Repository
@Transactional
public class RegionDAOImpl implements RegionDAO{
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Region> getRegions(Integer countryId) {
		return (List<Region>) sessionFactory.getCurrentSession()
				.createQuery("from Region where id_country = ?")
				.setLong(0, countryId).list();	
	}

	@Override
	public Region getRegion(Integer regionId) {
		return (Region) sessionFactory.getCurrentSession()
				.createQuery("from Region where id_region = ?")
				.setLong(0, regionId).uniqueResult();	
	}
}
