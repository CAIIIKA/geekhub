package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Message;
import com.geekhub.beans.User;

public interface MessageDAO {

	void addMessage(Message mess);

	List<Message> getMessageListReceiver(User user);
	
	List<Message> getMessageListOwner(User user);

	void deleteMessage(Integer messageId);

	User getOwnerByMessageId(Integer messageId);

	User getReceiverByMessageId(Integer messageId);

	List<Message> getMessages(User user1, User user2);

	List<Message> getMessagesInDialog(User user);
	
	void saveMessage(Message message);

	List<Message> getNewMessages(User user1, User user2);

	Integer getNewMessagesCount(User user);
	

	

}
