package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Friends;
import com.geekhub.beans.User;

public interface FriendsDAO {

	void save(Friends fr);


	
	public List<Friends> getFriends(User user);
	
	public Friends getFriends(User user1, User user2);
	public List<Integer> getFriendsIdsByUserId(Integer id);

	Friends getNotConfemedFriends(User user1,User user2);



	void deleteFriend(User user, User nonFriendUser);



	List<Friends> getInInvitations(User user);
	List<Friends> getOutInvitations(User user);



	void deleteFriendship(Friends fr);



	Integer getFriendsInvitationCount(User user);

}
