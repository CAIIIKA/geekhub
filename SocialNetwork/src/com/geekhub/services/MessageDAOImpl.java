package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Message;
import com.geekhub.beans.User;

@Repository
@Transactional
public class MessageDAOImpl implements MessageDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void addMessage(Message message) {
		sessionFactory.getCurrentSession().saveOrUpdate(message);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessageListReceiver(User user) {
		return (List<Message>) sessionFactory.getCurrentSession()
				.createQuery("from Message where RECEIVER_ID = ?")
				.setLong(0, user.getId()).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessageListOwner(User user) {
		return (List<Message>) sessionFactory.getCurrentSession()
				.createQuery("from Message where OWNER_ID = ?")
				.setLong(0, user.getId()).list();
	}

	public Message getMessage(Integer messageId) {
		return (Message) sessionFactory.getCurrentSession().get(Message.class,
				messageId);
	}

	@Override
	public void deleteMessage(Integer messageId) {
		Message message = getMessage(messageId);
		if (null != message)
			sessionFactory.getCurrentSession().delete(message);
	}

	@Override
	public User getOwnerByMessageId(Integer messageId) {
		Message message = getMessage(messageId);
		return message.getOwner();
	}

	@Override
	public User getReceiverByMessageId(Integer messageId) {
		Message message = getMessage(messageId);
		return message.getReceiver();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessages(User user1, User user2) {
		return (List<Message>) sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Message where (OWNER_ID = ? and RECEIVER_ID = ?) OR (RECEIVER_ID = ? and OWNER_ID = ?)")
				.setLong(0, user1.getId()).setLong(1, user2.getId())
				.setLong(2, user1.getId()).setLong(3, user2.getId()).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Message> getNewMessages(User user1, User user2) {
		return (List<Message>) sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Message where RECEIVER_ID = ? and OWNER_ID = ? and STATUS = 1")
				.setLong(0, user1.getId()).setLong(1, user2.getId()).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Message> getMessagesInDialog(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"select case when OWNER_ID= ? then RECEIVER_ID else OWNER_ID end from Message where ? in (OWNER_ID, RECEIVER_ID)")
				.setInteger(0, user.getId()).setInteger(1, user.getId()).list();
	}

	@Override
	public void saveMessage(Message message) {
		sessionFactory.getCurrentSession().saveOrUpdate(message);		
	}

	@Override
	public Integer getNewMessagesCount(User user) {
		@SuppressWarnings("unchecked")
		List<Message>  mess=  sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Message where RECEIVER_ID = ? and STATUS = 1")
				.setLong(0, user.getId()).list();
		return mess.size();
	}

}