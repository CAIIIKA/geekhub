package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.City;

public interface CityDAO {

	public List<City> getCities(Integer regionId);

	public City getCity(Integer cityId);


}
