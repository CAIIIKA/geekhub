package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Region;


public interface RegionDAO {

	List<Region> getRegions(Integer countryId);

	Region getRegion(Integer regionId);
	
}
