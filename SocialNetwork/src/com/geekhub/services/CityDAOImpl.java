package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.City;
import com.geekhub.beans.User;


@Repository
@Transactional
public class CityDAOImpl implements CityDAO{
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<City> getCities(Integer regionId) {
		return (List<City>) sessionFactory.getCurrentSession()
				.createQuery("from City where id_region = ?")
				.setLong(0, regionId).list();
	}

	@Override
	public City getCity(Integer cityId) {
		return (City)sessionFactory.getCurrentSession().get(City.class, cityId);
	}

	
}
