package com.geekhub.services;


import com.geekhub.beans.User;
import com.geekhub.beans.WallMessageComment;

public interface WallMessageCommentDAO {

	User getOwnerByMessageId(Integer messageId);

	WallMessageComment getWallMessageComment(Integer messageId);

	void deleteMessage(Integer messageId);

	void save(WallMessageComment wallMessageComment);

	

}
