package com.geekhub.services;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;
import com.geekhub.beans.WallMessageComment;

@Repository
@Transactional
public class WallMessageCommentDAOImpl implements WallMessageCommentDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public WallMessageComment getWallMessageComment(Integer messageId) {
		return (WallMessageComment) sessionFactory.getCurrentSession().get(WallMessageComment.class, messageId);
	}
	@Override
	public User getOwnerByMessageId(Integer messageId) {
		WallMessageComment wallMessageComment = getWallMessageComment(messageId);
		return wallMessageComment.getOwner();
	}
	@Override
	public void deleteMessage(Integer messageId) {
		WallMessageComment wallMessageComment = getWallMessageComment(messageId);
		if (null != wallMessageComment)
			sessionFactory.getCurrentSession().delete(wallMessageComment);	
		
	}
	@Override
	public void save(WallMessageComment wallMessageComment) {
		sessionFactory.getCurrentSession().saveOrUpdate(wallMessageComment);
		
	}
}