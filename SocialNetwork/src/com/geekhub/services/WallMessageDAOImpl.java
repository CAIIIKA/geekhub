package com.geekhub.services;

import java.util.Collection;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;
import com.geekhub.beans.WallMessageComment;

@Repository
@Transactional
public class WallMessageDAOImpl implements WallMessageDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	WallMessageCommentDAO wallMessageCommentDAO;
	
	@SuppressWarnings("unchecked")
	public List<WallMessage> getWallMessageList(User user) {
		return (List<WallMessage>) sessionFactory.getCurrentSession()
				.createQuery("from WallMessage where RECEIVER_ID = ?")
				.setLong(0, user.getId()).list();	
	}
	@Override
	public void addWallMessage(WallMessage message) {
		sessionFactory.getCurrentSession().saveOrUpdate(message);	
	}
	
	public WallMessage getWallMessage(Integer messageId) {
		return (WallMessage) sessionFactory.getCurrentSession().get(WallMessage.class, messageId);
	}
	
	@Override
	public void deleteMessage(Integer messageId) {
		WallMessage wallMessage = getWallMessage(messageId);
		Collection<WallMessageComment> wmcCollection= wallMessage.getComments();
		for(WallMessageComment wmc: wmcCollection){
			wallMessageCommentDAO.deleteMessage(wmc.getId());
		}
		if (null != wallMessage)
			sessionFactory.getCurrentSession().delete(wallMessage);	
	}
	@Override
	public User getOwnerByMessageId(Integer messageId) {
		WallMessage wallMessage = getWallMessage(messageId);
		return wallMessage.getOwner();
	}
	
	@Override
	public User getReceiverByMessageId(Integer messageId) {
		WallMessage wallMessage = getWallMessage(messageId);
		return wallMessage.getReceiver();
	}
	


}