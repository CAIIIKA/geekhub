package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Friends;
import com.geekhub.beans.Message;
import com.geekhub.beans.User;

@Repository
@Transactional
public class FriendsDAOImpl implements FriendsDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void save(Friends fr) {
		sessionFactory.getCurrentSession().saveOrUpdate(fr);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Friends> getFriends(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friends where (user_1_ID = ? OR user_2_ID =?) AND status='confimed'")
				.setInteger(0, user.getId()).setInteger(1, user.getId()).list();
	}

	@Override
	public Friends getNotConfemedFriends(User user1, User user2) {
		return (Friends) sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friends where (user_1_ID = ? AND user_2_ID =?) OR (user_1_ID = ? AND user_2_ID =?) AND status!='confemed'")
				.setInteger(0, user1.getId()).setInteger(1, user2.getId())
				.setInteger(2, user2.getId()).setInteger(3, user1.getId())
				.uniqueResult();

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getFriendsIdsByUserId(Integer id) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"select id from User where id in(select case when user_1_ID= ? then user_2 else user_1 end from Friends where ? in (user_1_ID, user_2_ID))")
				.setInteger(0, id).setInteger(1, id).list();
	}

	@Override
	public void deleteFriend(User user, User nonFriendUser) {
		Friends fr = getFriends(user, nonFriendUser);
		fr.setStatus("declined");
		if (user.getId()==fr.getUser_1().getId())
			fr.setNoFriendField(false);
		else
			fr.setNoFriendField(true);
	}

	@Override
	public Friends getFriends(User user1, User user2) {
		return (Friends) sessionFactory
		.getCurrentSession()
		.createQuery(
				"from Friends where (user_1_ID = ? AND user_2_ID =?) OR (user_1_ID = ? AND user_2_ID =?) AND status='confemed'")
		.setInteger(0, user1.getId()).setInteger(1, user2.getId())
		.setInteger(2, user2.getId()).setInteger(3, user1.getId()).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Friends> getInInvitations(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friends where (user_1_ID = ? AND NoFriendField = 0) OR (user_2_ID = ? AND NoFriendField = 1 ) AND status = 'waiting'")
				.setInteger(0, user.getId()).setInteger(1, user.getId())
				.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Friends> getOutInvitations(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friends where (user_1_ID = ? AND NoFriendField = 1) OR (user_2_ID = ? AND NoFriendField = 0 ) AND status = 'waiting'")
				.setInteger(0, user.getId()).setInteger(1, user.getId())
				.list();
	}

	@Override
	public void deleteFriendship(Friends fr) {
		sessionFactory.getCurrentSession().delete(fr);	
	}
	
	@Override
	public Integer getFriendsInvitationCount(User user) {
		List<Friends> fr = getInInvitations(user);
		return fr.size();
	}

}