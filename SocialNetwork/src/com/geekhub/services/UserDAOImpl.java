package com.geekhub.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserList() {
		return sessionFactory.getCurrentSession().createQuery("from User")
				.list();
	}

	@Override
	public User getUserById(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if (null != user)
			sessionFactory.getCurrentSession().delete(user);

	}

	@Override
	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);

	}

	@Override
	public User getUser(String login, String password) {
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User where login = ? AND password = ?")
				.setString(0, login).setString(1, password).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUsers(User user) {
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.like("name", user.getName() + "%"))
				.add(Restrictions.like("surname", user.getSurname() + "%"))
				.add(Restrictions.like("patronymic", user.getPatronymic() + "%"));
		if(user.getCountry()!=null)
			criteria.add(Restrictions.eq("country",user.getCountry()));
		if(user.getRegion()!=null)
			criteria.add(Restrictions.eq("region",user.getRegion()));
		if(user.getCity()!=null)
			criteria.add(Restrictions.eq("city",user.getCity()));
		return criteria.list();
	}

	@Override
	public User getUserByUsername(String login) {
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User where login = ?").setString(0, login)
				.uniqueResult();
	}
}
