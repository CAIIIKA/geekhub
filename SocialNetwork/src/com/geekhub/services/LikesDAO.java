package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Likes;
import com.geekhub.beans.User;

public interface LikesDAO {

	void like(Integer id, User user, String item);

	List<Likes> getLikes(Integer id, String item);

}
