package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;

public interface WallMessageDAO {

	void addWallMessage(WallMessage wallMessage);

	List<WallMessage> getWallMessageList(User user);

	void deleteMessage(Integer messageId);
	
	User getOwnerByMessageId(Integer messageId);

	User getReceiverByMessageId(Integer messageId);
	
	WallMessage getWallMessage(Integer messageId);

}
