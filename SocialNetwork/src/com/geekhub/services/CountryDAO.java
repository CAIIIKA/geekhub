package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Country;


public interface CountryDAO {
	
	public List<Country> getListCountries();
	Country getCountry(Integer id);
}
