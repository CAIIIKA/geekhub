package com.geekhub.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class BaseController {

	@Autowired
	UserDAO userDAO;

	@RequestMapping(value = "log.html")
	public String login(HttpSession session, HttpServletResponse response, HttpServletRequest request){	
		String cookie = null;
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Cookie[] cookies= request.getCookies();
		for (int i = 0; i<cookies.length; i++)
			if (cookies[i].getName().equals(username)){
				cookie = cookies[i].getValue();
				break;
			}

		if (cookie == null)
			response.addCookie(new Cookie(username,"1"));
		else {
			Integer count = Integer.parseInt(cookie);
			if (count < 2) {
				count++;
				response.addCookie(new Cookie(username,count.toString()));
				session.setAttribute("showRecommended", 0);
			}
			else {
				response.addCookie(new Cookie(username,"1"));
				session.setAttribute("showRecommended", 1);
			}
		}
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		session.setAttribute("loggedUser", user);
		return "redirect:./index.html";
	}

	@RequestMapping(value = "index.html")
	public String getIndex(HttpSession session) {
		return "redirect:./getPage.html?id="
				+ ((User) session.getAttribute("loggedUser")).getId();
	}

}
