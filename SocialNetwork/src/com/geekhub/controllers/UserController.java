package com.geekhub.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.City;
import com.geekhub.beans.Country;
import com.geekhub.beans.Friends;
import com.geekhub.beans.Region;
import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;
import com.geekhub.services.CityDAO;
import com.geekhub.services.CountryDAO;
import com.geekhub.services.FriendsDAO;
import com.geekhub.services.UserDAO;
import com.geekhub.services.WallMessageDAO;
import com.geekhub.services.RegionDAO;

import java.util.Date;

@Controller
public class UserController {

	@Autowired
	UserDAO userDAO;
	@Autowired
	CountryDAO countryDAO;
	@Autowired
	WallMessageDAO wallMessageDAO;
	@Autowired
	RegionDAO regionDAO;
	@Autowired
	CityDAO cityDAO;

	@Autowired
	FriendsDAO friendsDAO;

	@Autowired
	@Qualifier("org.springframework.security.authenticationManager")
	AuthenticationManager authenticationManager;

	@RequestMapping(value = "/reg.action")
	public String reg(ModelMap map, HttpSession session, User newUser,
			@RequestParam(value = "Bdate", required = false) String date)
			throws ParseException, IOException {

		List<Country> countries = countryDAO.getListCountries();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		map.put("countries", countries);

		if (newUser.getName() == null) {
			newUser = new User();
			map.put("newUser", newUser);
			return "registration";
		}

		if (!date.equals("")) {
			Date birthday = formatter.parse(date);
			newUser.setDate(birthday);
		}

		newUser.setRoles("user");

		BufferedImage img = ImageIO.read(new File("C://camera.gif"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "gif", baos);
		byte[] imageInByte = baos.toByteArray();

		newUser.setAvatar(imageInByte);

		if (newUser.getCountry() == null)
			newUser.setCountry(null);

		String password = newUser.getPassword();
		PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		String hashedPassword = passwordEncoder.encodePassword(password, null);
		newUser.setPassword(hashedPassword);

		userDAO.saveUser(newUser);
		map.put("newUser", null);

		session.setAttribute("loggedUser", newUser);
		Authentication token = new UsernamePasswordAuthenticationToken(
				newUser.getLogin(), password);
		Authentication authentication = (Authentication) authenticationManager
				.authenticate(token);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		return "redirect:./index.html";
	}

	@RequestMapping(value = "/getFinded.html")
	public @ResponseBody
	String getFinded(
			ModelMap map,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "patronymic", required = false) String patronymic,
			@RequestParam(value = "countryId", required = false) Integer countryId,
			@RequestParam(value = "regionId", required = false) Integer regionId,
			@RequestParam(value = "cityId", required = false) Integer cityId) {
		List<Country> countries = countryDAO.getListCountries();
		map.put("countries", countries);

		if (name == null)
			name = "";
		if (surname == null)
			surname = "";
		if (patronymic == null)
			patronymic = "";

		User user = new User();
		user.setName(name + "");
		user.setSurname("");
		user.setPatronymic("");
		if (countryId != null)
			user.setCountry(countryDAO.getCountry(countryId));
		if (regionId != null)
			user.setRegion(regionDAO.getRegion(regionId));
		if (cityId != null)
			user.setCity(cityDAO.getCity(cityId));
		List<User> users = userDAO.findUsers(user);
		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "adress", "login",
				"password", "authorities", "accountNonExpired",
				"accountNonLocked", "credentialsNonExpired", "date", "roles",
				"email" };
		conf.setExcludes(excludes);

		JSONArray arr = JSONArray.fromObject(users, conf);

		return arr.toString();

	}

	@RequestMapping(value = "/find.html")
	public String find(ModelMap map) {
		List<Country> countries = countryDAO.getListCountries();
		map.put("countries", countries);
		return "findUser";

	}

	@RequestMapping(value = "/addFriend.html")
	public String addFriend(ModelMap map, HttpSession session,
			@RequestParam(value = "userId", required = true) Integer userId) {
		User user2 = userDAO.getUserById(userId);

		User user1 = (User) session.getAttribute("loggedUser");
		Friends fr1;

		fr1 = friendsDAO.getNotConfemedFriends(user1, user2);
		if (fr1 != null) {
			if (fr1.isNoFriendField() == true)
				if (fr1.getUser_1().getId() != user1.getId())
					fr1.setStatus("confimed");

			if (fr1.isNoFriendField() == false)
				if (fr1.getUser_2().getId() != user1.getId())
					fr1.setStatus("confimed");
			friendsDAO.save(fr1);
		} else {
			fr1 = new Friends();
			fr1.setUser_1(user1);
			fr1.setUser_2(user2);
			fr1.setStatus("waiting");
			fr1.setNoFriendField(true);
			friendsDAO.save(fr1);
		}
		return "redirect:./getPage.html?id=" + userId;

	}

	@RequestMapping(value = "/deleteFriend.html")
	public String deleteFriend(ModelMap map, HttpSession session,
			@RequestParam(value = "userId", required = true) Integer userId) {
		User user = (User) session.getAttribute("loggedUser");
		User nonFriendUser = userDAO.getUserById(userId);
		friendsDAO.deleteFriend(user, nonFriendUser);
		return "redirect:./getFriendsPage.html";
	}
	
	
	@RequestMapping(value = "/getFriendsPage.html")
	public String getFriendsPage(ModelMap map, HttpSession session) {
		return "myFriends";
	}
	
	@RequestMapping(value = "/getFriends.html")
	public @ResponseBody String getFriend(ModelMap map, HttpSession session) {

		User user = (User) session.getAttribute("loggedUser");
		List<Friends> fr = friendsDAO.getFriends(user);
		List<User> myFriends = new ArrayList<User>();
		for (Friends tmp : fr) {
			if (tmp.getUser_1().getId() != user.getId())
				myFriends.add(tmp.getUser_1());
			else
				myFriends.add(tmp.getUser_2());
		}
		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "adress", "login",
				"password", "authorities", "accountNonExpired",
				"accountNonLocked", "credentialsNonExpired", "date", "roles",
				"email","city","region","country" };
		

		JSONArray arr = JSONArray.fromObject(myFriends, conf);

		return arr.toString();
	}
	
	@RequestMapping(value = "/getInInvitations.html")
	public @ResponseBody String getInInvitations(ModelMap map, HttpSession session) {

		User user = (User) session.getAttribute("loggedUser");
		List<Friends> fr = friendsDAO.getInInvitations(user);
		List<User> myFriends = new ArrayList<User>();
		for (Friends tmp : fr) {
			if (tmp.getUser_1().getId() != user.getId())
				myFriends.add(tmp.getUser_1());
			else
				myFriends.add(tmp.getUser_2());
		}
		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "adress", "login",
				"password", "authorities", "accountNonExpired",
				"accountNonLocked", "credentialsNonExpired", "date", "roles",
				"email","city","region","country" };
		conf.setExcludes(excludes);

		JSONArray arr = JSONArray.fromObject(myFriends, conf);

		return arr.toString();
	}
	
	@RequestMapping(value = "/getOutInvitations.html")
	public @ResponseBody String getOutInvitations(ModelMap map, HttpSession session) {

		User user = (User) session.getAttribute("loggedUser");
		List<Friends> fr = friendsDAO.getOutInvitations(user);
		List<User> myFriends = new ArrayList<User>();
		for (Friends tmp : fr) {
			if (tmp.getUser_1().getId() != user.getId())
				myFriends.add(tmp.getUser_2());
			else
				myFriends.add(tmp.getUser_1());
		}
		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "adress", "login",
				"password", "authorities", "accountNonExpired",
				"accountNonLocked", "credentialsNonExpired", "date", "roles",
				"email","city","region","country" };
		conf.setExcludes(excludes);

		JSONArray arr = JSONArray.fromObject(myFriends, conf);

		return arr.toString();
	}
	
	@RequestMapping(value = "/cancleInvitations.html")
	public String cancleInvitations(ModelMap map, HttpSession session, 
			@RequestParam(value = "userId", required = true) Integer userId) {

		User user1 = (User) session.getAttribute("loggedUser");
		User user2 = userDAO.getUserById(userId);
		Friends fr = friendsDAO.getFriends(user1, user2);
		friendsDAO.deleteFriendship(fr);
		return "redirect:./getFriendsPage.html";	
	}
	
	@RequestMapping(value = "/getFriendsInvitationCount.html")
	public @ResponseBody String getNewMessagesCount(HttpSession session) {
		Integer count = friendsDAO.getFriendsInvitationCount((User)session.getAttribute("loggedUser"));
		
		JSONObject json = new JSONObject();
		json.accumulate("count", count);
		return json.toString();
	}

	@RequestMapping(value = "/getRecomendedFriends.html")
	public @ResponseBody
	String getRecomendedFriend(ModelMap map, HttpSession session) {
		User user = (User) session.getAttribute("loggedUser");

		Queue<Integer> q = new LinkedList<Integer>();
		List<Integer> friendsIds = friendsDAO.getFriendsIdsByUserId(user
				.getId());
		List<Integer> visited = new ArrayList<Integer>();
		List<Integer> recomended = new ArrayList<Integer>();
		boolean flag = true;

		visited.add(user.getId());
		visited.addAll(friendsIds);
		q.addAll(friendsIds);
		int count = 0;

		while (!q.isEmpty() && count <= 10) {
			Integer greyNode = q.poll();
			List<Integer> friendsFriendsIds = friendsDAO
					.getFriendsIdsByUserId(greyNode);
			Iterator<Integer> iterator = friendsFriendsIds.iterator();
			while (iterator.hasNext() && count <= 10) {
				Integer nextNode = iterator.next();
				for (Integer vis : visited) {
					if (vis == nextNode || nextNode == user.getId()) {
						flag = false;
						break;
					}
				}
				if (flag == true) {
					recomended.add(nextNode);
					q.add(nextNode);
					visited.add(nextNode);
					count++;
				}
				flag = true;
			}
		}

		List<User> myRecomendedFriends = new ArrayList<User>();
		for (Integer id : recomended)
			myRecomendedFriends.add(userDAO.getUserById(id));

		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "city", "region",
				"country", "adress", "login", "password", "authorities",
				"accountNonExpired", "accountNonLocked",
				"credentialsNonExpired", "date", "roles", "email" };
		conf.setExcludes(excludes);

		JSONArray arr = JSONArray.fromObject(myRecomendedFriends, conf);

		return arr.toString();
	}

	@RequestMapping(value = "/edit.html")
	public String edit(ModelMap map, User user, HttpSession session,
			@RequestParam(value = "date", required = false) String date)
			throws ParseException {

		List<Country> countries = countryDAO.getListCountries();
		map.put("countries", countries);

		User loggedUser = (User) session.getAttribute("loggedUser");
		if (user.getName() == null) {

			map.put("user", loggedUser);
			if (loggedUser.getCountry() != null) {
				List<Region> regions = regionDAO.getRegions(loggedUser
						.getCountry().getId());
				map.put("regions", regions);
				if (loggedUser.getRegion() != null) {
					List<City> cities = cityDAO.getCities(loggedUser
							.getRegion().getId());
					map.put("cities", cities);
				}
			}

			return "editInfo";
		}
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		if (!date.equals("")) {
			Date birthday = formatter.parse(date);
			user.setDate(birthday);
		};
		user.setAvatar(loggedUser.getAvatar());
		user.setRoles(loggedUser.getRoles());
		user.setLogin(loggedUser.getLogin());
		user.setId(loggedUser.getId());
		userDAO.saveUser(user);
		map.put("user", null);
		return "redirect:/getPage.html?id=" + loggedUser.getId();
	}

	@RequestMapping(value = "/getPage.html")
	public String getUserPage(ModelMap map, HttpSession session,
			HttpServletResponse response,
			@RequestParam(value = "id", required = true) Integer id) {
		User user = userDAO.getUserById(id);
		map.put("getedUser", user);
		List<WallMessage> wallMessages = wallMessageDAO
				.getWallMessageList(user);
		Collections.reverse(wallMessages);
		map.put("wallMessages", wallMessages);
	
		User loggedUser = (User) session.getAttribute("loggedUser");
		map.put("loggedUser", loggedUser);

		boolean areFriends = false;
		if(friendsDAO.getFriends(user, loggedUser)!=null)
			areFriends = true;	
		map.put("areFriends", areFriends);
		return "index";
	}

	@RequestMapping(value = "/getRegions.action", method = RequestMethod.GET)
	public @ResponseBody
	String regions(
			HttpServletResponse response,
			@RequestParam(value = "countryId", required = true) Integer countryId)
			throws IOException {

		response.setContentType("application/json");
		List<Region> reg = regionDAO.getRegions(countryId);

		JSONArray arr = JSONArray.fromObject(reg);
		return arr.toString();
	}

	@RequestMapping(value = "/getCities.action", method = RequestMethod.GET)
	public @ResponseBody
	String cities(HttpServletResponse response,
			@RequestParam(value = "regionId", required = true) Integer regionId)
			throws IOException {

		response.setContentType("application/json");
		List<City> cities = cityDAO.getCities(regionId);
		JSONArray arr = JSONArray.fromObject(cities);

		return arr.toString();
	}
}
