package com.geekhub.controllers;


import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;
import com.geekhub.beans.WallMessageComment;
import com.geekhub.services.CountryDAO;
import com.geekhub.services.UserDAO;
import com.geekhub.services.WallMessageCommentDAO;
import com.geekhub.services.WallMessageDAO;

@Controller
public class WallController {

	@Autowired
	UserDAO userDAO;
	@Autowired
	CountryDAO countryDAO;
	@Autowired
	WallMessageDAO wallMessageDAO;
	@Autowired
	WallMessageCommentDAO wallMessageCommentDAO;

	@RequestMapping(value = "/addWallMessage.action")
	public @ResponseBody
	String addWallMessage(
			ModelMap map,
			HttpSession session, HttpServletRequest request,
			@RequestParam(value = "message", required = true) String message,
			@RequestParam(value = "receiverId", required = true) Integer receiverId) throws UnsupportedEncodingException {
   
        
		System.out.print("\n\n\n"+message+"\n\n\n\n");
		User user = (User) session.getAttribute("loggedUser");
		WallMessage wallMessage = new WallMessage();
		wallMessage.setMessage(message);
		wallMessage.setOwner(user);
		wallMessage.setReceiver(userDAO.getUserById(receiverId));
		wallMessageDAO.addWallMessage(wallMessage);

		JsonConfig conf = new JsonConfig();


	    final String[] excludes = new String[]{
	    	"avatar",
	    	"city",
	    	"region",
	    	"country",
	    	"adress",
	    	"login",
	    	"password",
	    	"authorities",
	    	"accountNonExpired",
	    	"accountNonLocked",
	    	"credentialsNonExpired",
	    	"date",
	    	"roles",
	    	"email"
	    };
	    conf.setExcludes(excludes);

		//conf.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		JSONObject json = JSONObject.fromObject(wallMessage, conf);

		return json.toString();
	}

	@RequestMapping(value = "/addWallMessageComment.action")
	public @ResponseBody String addComment(
			ModelMap map,
			HttpServletRequest request,
			@RequestParam(value = "message", required = true) String message,
			@RequestParam(value = "wallMessageId", required = true) Integer wallMessageId) {
		
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		User user = userDAO.getUserByUsername(username);
		WallMessage wm = wallMessageDAO.getWallMessage(wallMessageId);
		WallMessageComment wallMessageComment = new WallMessageComment();
		wallMessageComment.setMessage(message);
		wallMessageComment.setOwner(user);
		wallMessageComment.setWallMessage(wm);
		wallMessageCommentDAO.save(wallMessageComment);
		JsonConfig conf = new JsonConfig();
	
		  final String[] excludes = new String[]{
			    	"avatar",
			    	"city",
			    	"region",
			    	"country",
			    	"adress",
			    	"login",
			    	"password",
			    	"authorities",
			    	"accountNonExpired",
			    	"accountNonLocked",
			    	"credentialsNonExpired",
			    	"date",
			    	"roles",
			    	"email",
			    	"wallMessage"
			    };
			    conf.setExcludes(excludes);
		
		JSONObject json = JSONObject.fromObject(wallMessageComment,conf);
		return json.toString();
	}

	@RequestMapping(value = "/deleteWallMessage.action")
	public String deledeMessage(ModelMap map, HttpServletRequest request,
			@RequestParam(value = "id", required = false) Integer messageId) {
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		User user = userDAO.getUserByUsername(username);
		map.put("loggedUser", user);

		User owner = wallMessageDAO.getOwnerByMessageId(messageId);
		User receiver = wallMessageDAO.getReceiverByMessageId(messageId);
		if ((owner.getId() == user.getId())
				|| (user.getId() == receiver.getId()))
			wallMessageDAO.deleteMessage(messageId);
		return "redirect:./getPage.html?id=" + receiver.getId();
	}

	@RequestMapping(value = "/deleteWallMessageComment.action")
	public String deledeComment(ModelMap map,
			@RequestParam(value = "id", required = false) Integer messageId) {
		String username = SecurityContextHolder.getContext()
				.getAuthentication().getName();
		User user = userDAO.getUserByUsername(username);
		map.put("loggedUser", user);

		User owner = wallMessageCommentDAO.getOwnerByMessageId(messageId);
		WallMessageComment wmc = wallMessageCommentDAO
				.getWallMessageComment(messageId);
		User receiver = (wallMessageDAO.getWallMessage((wmc.getWallMessage()
				.getId())).getReceiver());
		if ((owner.getId() == user.getId())
				|| (user.getId() == receiver.getId()))
			wallMessageCommentDAO.deleteMessage(messageId);
		return "redirect:./getPage.html?id=" + receiver.getId();
	}

}
