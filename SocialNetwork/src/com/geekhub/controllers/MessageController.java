package com.geekhub.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.Message;
import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;
import com.geekhub.services.CountryDAO;
import com.geekhub.services.UserDAO;
import com.geekhub.services.MessageDAO;

@Controller
public class MessageController {

	@Autowired
	UserDAO userDAO;
	@Autowired
	CountryDAO countryDAO;
	@Autowired
	MessageDAO messageDAO;

	@RequestMapping(value = "/getDialog.html")
	public String getUserPage(ModelMap map, HttpSession session,
			@RequestParam(value = "id", required = true) Integer id) {
		User user = userDAO.getUserById(id);
		map.put("getedUser", user);
		List<Message> messages = messageDAO.getMessages((User)session.getAttribute("loggedUser"),user);
		
		for(Message m :messages){
			if (m.getStatus()==true && m.getReceiver().getId()==((User)session.getAttribute("loggedUser")).getId()){
				m.setStatus(false);
				messageDAO.saveMessage(m);
			}
		}
			
		Collections.reverse(messages);
		map.put("messages", messages);

		User loggedUser = (User) session.getAttribute("loggedUser");
		map.put("loggedUser", loggedUser);

		return "dialog";
	}
	
	@RequestMapping(value = "/getNewMessages.html")
	public @ResponseBody String getNewMessages(HttpSession session,
			@RequestParam(value = "id", required = true) Integer id) {
		User user = userDAO.getUserById(id);
		List<Message> messages = messageDAO.getNewMessages((User)session.getAttribute("loggedUser"),user);
		
		for(Message m :messages){
			m.setStatus(false);
			messageDAO.saveMessage(m);
		}
		
		
		JsonConfig conf = new JsonConfig();

		final String[] excludes = new String[] { "avatar", "adress", "login",
				"password", "authorities", "accountNonExpired",
				"accountNonLocked", "credentialsNonExpired", "date", "roles",
				"email" };
		conf.setExcludes(excludes);

		JSONArray arr = JSONArray.fromObject(messages, conf);

		return arr.toString();
	}
	
	
	@RequestMapping(value = "/getNewMessagesCount.html")
	public @ResponseBody String getNewMessagesCount(HttpSession session) {
		Integer count = messageDAO.getNewMessagesCount((User)session.getAttribute("loggedUser"));
		
		JSONObject json = new JSONObject();
		json.accumulate("count", count);
		return json.toString();
	}
	
	

	@RequestMapping(value = "/sendMessage.html")
	public @ResponseBody String sendMessage(
			ModelMap map,
			HttpServletRequest request, HttpSession session,
			@RequestParam(value = "message", required = false) String message,
			@RequestParam(value = "receiverId", required = false) Integer receiverId) {
		
		User user = (User)session.getAttribute("loggedUser");
		Message mess = new Message();
		mess.setMessage(message);
		mess.setOwner(user);
		mess.setStatus(true);
		mess.setReceiver(userDAO.getUserById(receiverId));
		
		messageDAO.addMessage(mess);
		
		JsonConfig conf = new JsonConfig();

		//conf.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		JSONObject json = JSONObject.fromObject(mess, conf);
		
		return json.toString();
	}

	@RequestMapping(value = "/myInboxMessages.html")
	public String myInboxMessages(
			ModelMap map,
			HttpServletRequest request, HttpSession session) {
		
		User user = (User)session.getAttribute("loggedUser");	
		List<Message> messages = messageDAO.getMessageListReceiver(user);
		Collections.reverse(messages);
		map.put("messages", messages);		
		return "myMessages";
	}
	
	@RequestMapping(value = "/myOutboxMessages.html")
	public String myOutboxMessages(
			ModelMap map,
			HttpServletRequest request, HttpSession session) {
		
		User user = (User)session.getAttribute("loggedUser");	
		List<Message> messages = messageDAO.getMessageListOwner(user);
		Collections.reverse(messages);
		map.put("messages", messages);		
		return "myMessages";
	}

}
