package com.geekhub.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;

import java.io.IOException;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class ImageController {

	@Autowired
	UserDAO userDAO;

	@RequestMapping(value = "/uploadAvatarPage.html")
	public String uploadAvatarPage() throws IOException {
		return "loadAvatar";
	}

	@RequestMapping(value = "/uploadAvatar.html")
	public String processUploadPreview(@RequestParam(value = "file", required = true) MultipartFile file, HttpSession session)
			throws IOException {
		file.getInputStream();
		file.getContentType();
		file.getSize();
		file.getOriginalFilename();
	

		byte[] bFile = file.getBytes();
		User user = (User)session.getAttribute("loggedUser");
		user.setAvatar(bFile);
		userDAO.saveUser(user);
		return "redirect:./index.html";
	}

	@RequestMapping(value = "/getUsersAvatar.html")
	public void uploadAvatarPage(ModelMap map, HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "id", required = true) Integer id) {
		User user = userDAO.getUserById(id);
		byte[] ava = user.getAvatar();
		String name = "userAvatar";
		response.setContentType("image/jpeg");
		response.setContentLength(ava.length);

		response.setHeader("Content-Disposition", "inline; filename=\"" + name
				+ "\"");

		BufferedInputStream input = null;
		BufferedOutputStream output = null;

		try {
			input = new BufferedInputStream(new ByteArrayInputStream(ava));
			output = new BufferedOutputStream(response.getOutputStream());
			byte[] buffer = new byte[8192];
			int length;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
		} catch (IOException e) {
			System.out
					.println("There are errors in reading/writing image stream "
							+ e.getMessage());
		} finally {
			if (output != null)
				try {
					output.close();
				} catch (IOException ignore) {
				}
			if (input != null)
				try {
					input.close();
				} catch (IOException ignore) {
				}
		}
	}	
}
