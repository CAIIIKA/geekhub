package com.geekhub.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Likes;
import com.geekhub.beans.User;
import com.geekhub.beans.WallMessage;
import com.geekhub.services.LikesDAO;
import com.geekhub.services.UserDAO;
import com.geekhub.services.WallMessageDAO;

@Controller
public class LikeController {

	@Autowired
	LikesDAO likesDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	WallMessageDAO wallMessageDAO;

	@RequestMapping(value = "/like.html")
	public void like(HttpSession session, 
			@RequestParam (value = "id", required = true) Integer id,
			@RequestParam (value = "item", required = true) String item){
		
		System.out.print("\n\n\n\n � ��� ");
		User user = (User) session.getAttribute("loggedUser");
		likesDAO.like(id, user, item);
		
		
	}
	
	@RequestMapping(value = "/getCommentLikes.html")
	public String commentLikes(HttpSession session, ModelMap map, 
			@RequestParam (value = "id", required = true) Integer id,
			@RequestParam (value = "item", required = true) String item){
		
		List <Likes> likes = likesDAO.getLikes(id, item);
		List<User> likedUsers = new ArrayList<User>();
		for (Likes comentLike: likes){
			likedUsers.add(comentLike.getUser()); 
		}
		map.put("likedUsers", likedUsers);
		return "likes";	
	}
}
