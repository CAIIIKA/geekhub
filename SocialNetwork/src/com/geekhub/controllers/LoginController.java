package com.geekhub.controllers;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;

import com.geekhub.services.UserDAO;

@Controller
public class LoginController {

	@Autowired
	UserDAO userDAO;

	@RequestMapping(value="login.action")
	public String login (ModelMap map){
		return "login";
	}
	
	@RequestMapping(value="/logout.html")
	public String logout(HttpServletRequest request) {
		SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
		return "redirect:./login.action";
	}
}
