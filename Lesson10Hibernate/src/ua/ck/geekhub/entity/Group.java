package ua.ck.geekhub.entity;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="GROUPS")
public class Group {
	@Id
	@Column(name="GROUP_ID")
	@GeneratedValue
	private Integer id;
	
	@Column
	private String groupName;
	
	@OneToMany(mappedBy =  "group")
	private Set<User> users;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public Set<User> getUsers(){
		return users;
	}
	public void setUsers(Set<User> users){
		this.users = users;
	}
}
