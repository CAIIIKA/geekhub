package ua.ck.geekhub.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="USER")
public class User {

	@Id
	@Column(name="USER_ID")
	@GeneratedValue
	private Integer id;
	
	@Column
	private String userName;
	
	@Column
	private String email;
	
	@ManyToOne
	@JoinColumn(name="GROUP_ID")
	private Group group;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	
	public Group getGroup(){
		return group;
	}
	public void setGroup(Group group){
		this.group = group;
	}
	
}
