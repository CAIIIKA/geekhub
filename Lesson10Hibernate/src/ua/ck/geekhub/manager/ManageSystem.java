package ua.ck.geekhub.manager;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ua.ck.geekhub.HibernateUtil;
import ua.ck.geekhub.entity.Group;

import ua.ck.geekhub.entity.User;

public class  ManageSystem {
	
	private static  ManageSystem instance;
	private  ManageSystem() { }
	
	public static  ManageSystem getInstance() {
		if(instance == null) 
			instance = new  ManageSystem();
		return instance;
	}
	
   public void addUser(User user) {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   session.save(user);
	   session.getTransaction().commit();
	   session.close();
   }
   public void addGroup(Group group) {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   session.save(group);
	   session.getTransaction().commit();
	   session.close();
   }
   
   public List<Group> getGroups() {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   Criteria cr = session.createCriteria(Group.class);
	   session.getTransaction().commit();
	   @SuppressWarnings("unchecked")
	   List<Group> gr = cr.list();
	   session.close();
	   return gr;
   }
   
   public List<User> getUsersByGroup(Group group) {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   Criteria cr = session.createCriteria(User.class).add(Restrictions.eq("group", group));
	   session.getTransaction().commit();
	   @SuppressWarnings("unchecked")
	   List<User> ur = cr.list();
	   session.close();
	   return ur;
   }
   
   public Group getGroupById(Integer id) {
	   Session session = HibernateUtil.getSessionFactory().openSession();
	   session.beginTransaction();
	   Criteria cr = session.createCriteria(Group.class).add(Restrictions.eq("id", id));
	   Group gr = (Group) cr.list().get(0);
	   session.getTransaction().commit();
	   session.close();
	   return gr;
   }
   
   
   
 

}
