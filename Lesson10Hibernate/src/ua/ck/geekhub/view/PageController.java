package ua.ck.geekhub.view;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;
import ua.ck.geekhub.manager.ManageSystem;

@Controller
public class PageController {
	@RequestMapping("/index")
	public ModelAndView mainPage(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("main");
		Integer groupId = null;
		Group gr = null;
		List<Group> groups = null;
		List<User> users = null;
		// ***********add new group************
		if (request.getParameter("action") != null)
			if (request.getParameter("action").equals("Add group")) {
				gr = new Group();
				gr.setGroupName(request.getParameter("newGroup"));
				ManageSystem.getInstance().addGroup(gr);
				groups = ManageSystem.getInstance().getGroups();
				mav.addObject("groups", groups);
				return mav;
			}
		// ************************************

		// ***get users from selected group****
		if (request.getParameter("groupId") != null) {
			groupId = Integer.parseInt(request.getParameter("groupId"));
			gr = ManageSystem.getInstance().getGroupById(groupId);
			users = ManageSystem.getInstance().getUsersByGroup(gr);
		}
		groups = ManageSystem.getInstance().getGroups();

		mav.addObject("groups", groups);
		mav.addObject("users", users);
		mav.addObject("selectedGroup", gr);
		return mav;
		// ***********************************
	}

	// add new user
	@RequestMapping("/add")
	public ModelAndView addUser(HttpServletRequest request,
			HttpServletResponse response) {

		List<Group> groups = null;

		if (request.getParameter("action") == null) {
			ModelAndView mav = new ModelAndView("user");
			groups = ManageSystem.getInstance().getGroups();
			mav.addObject("groups", groups);
			return mav;
		}
		ModelAndView mav = new ModelAndView("main");
		User user = new User();
		Integer groupId = null;
		Group gr = null;

		if (request.getParameter("groupId") != null) {
			groupId = Integer.parseInt(request.getParameter("groupId"));
			gr = ManageSystem.getInstance().getGroupById(groupId);
		}
		groups = ManageSystem.getInstance().getGroups();
		mav.addObject("groups", groups);
		user.setUserName(request.getParameter("userName"));
		user.setEmail(request.getParameter("email"));
		user.setGroup(gr);
		ManageSystem.getInstance().addUser(user);
		return mav;
	}
}
