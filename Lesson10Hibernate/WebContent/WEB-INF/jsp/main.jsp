<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="./index">
		<table>
			<tr>
				<td><a href="./add">Add new user</a> <br></td>
			</tr>
			<tr>
				<td>Groups list:
			</tr>
			<tr>

				<td><select name="groupId">
						<c:forEach var="group" items="${groups}">
							<option value="${group.id}">
								<c:out value="${group.groupName}" />
							</option>
						</c:forEach>
				</select></td>
				<td><input type="submit" name="action" value="Get users" /></td>

			</tr>
			<tr>
				<td><input type="text" name="newGroup" value="${newGroup}" /></td>
				<td><input type="submit" name="action" value="Add group" /></td>
			</tr>


			<tr>
				<td>Selected group:<c:out value="${selectedGroup.groupName}" /></td>
			</tr>
		</table>
	</form>
	<table border = 1>
		<tr>
			<td>Name:</td>
			<c:forEach var="user" items="${users}">
				<td><c:out value="${user.userName}" /></td>
			</c:forEach>
		</tr>
		<tr>
			<td>e-mail:</td>
			<c:forEach var="user" items="${users}">
				<td><c:out value="${user.email}" /></td>
			</c:forEach>
		</tr>
	</table>
</body>
</html>