<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users</title>
</head>
<body>
	Add user:
	<br>
	<form action="./add">
		<table>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="userName" value="${userName}" /></td>
			</tr>
			<tr>
				<td>email:</td>
				<td><input type="text" name="email" value="${email}" /></td>
			</tr>
			<tr>
				<td>Select group:</td>
				<td><select name="groupId">
						<c:forEach var="group" items="${groups}">
							<option value="${group.id}">
								<c:out value="${group.groupName}" />
							</option>
						</c:forEach>
				</select><br></td>
			</tr>
			<tr>
				<td><input type="submit" name="action" value="Add" />
				<td>
			</tr>

		</table>
	</form>
</body>
</html>