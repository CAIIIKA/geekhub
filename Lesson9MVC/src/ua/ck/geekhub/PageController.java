package ua.ck.geekhub;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

	@RequestMapping("/session")
	public ModelAndView page(HttpServletRequest request,
			HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("main");
		
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		
		if (action == null) {
			return mav;
		}
		
		if (action.equals("add")) {
			Enumeration<String> atr = session.getAttributeNames();
			while (atr.hasMoreElements()) {
				if (atr.nextElement().equals(name)) {
					@SuppressWarnings("unchecked")
					List<String> sesAtr = (ArrayList<String>) session
							.getAttribute(name);
					sesAtr.add(value);
					session.setAttribute(name, sesAtr);
					return mav;
				}
			}
			List<String> sesAtr = new ArrayList<String>();
			sesAtr.add(value);
			session.setAttribute(name, sesAtr);
		}
		
		if (action.equals("remove")) {
			@SuppressWarnings("unchecked")
			List<String> sesAtr = (ArrayList<String>) session
					.getAttribute(name);
			sesAtr.remove(value);
			if (sesAtr.isEmpty())
				session.removeAttribute(name);
			else
				session.setAttribute(name, sesAtr);
			return mav;
		}
		return mav;
	}

}