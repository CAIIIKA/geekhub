
<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c"%>

<html>
<head>
<title>Session</title>
<style type="text/css">
body,table,hr {
	color: black;
	background: silver;
}
</style>
</head>
<body>
	<form action="./session">
		<br>
		<table border="1">
			<tr>
				<td><c:out value="Name: " /></td>
				<td><c:out value="Value: " /></td>
				<td><c:out value="Action: " /></td>
			</tr>

	
			<c:forEach items='${sessionScope}' var='p'>
				<tr>
				<tr>
					<td><c:out value='${p.key}' /></td>
					<td><c:out value='${p.value[0]}' /></td>
					<td><a href="./session?action=remove&value=${p.value[0]}&name=${p.key}">delete</a></td>
				</tr>
				<c:forEach items='${p.value}' var='k' begin="1">
					<tr>
						<td><br></td>
						<td><c:out value='${k}' /></td>
						<td><a href="./session?action=remove&value=${k}&name=${p.key}">delete</a></td>
					</tr>
				</c:forEach>
			</c:forEach>
			<tr>
				<td><input type="text" size="30" name="name"></td>
				<td><input type="text" size="30" name="value"></td>
				<td> <input type="submit" name="action" value="add"> </td>
			</tr>
		</table>
	</form>
</body>
</html>