package ua.ck.geekhub.clone;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;

public class CloneObject {

	public static Object clone(Object obj) throws IllegalArgumentException,
			IllegalAccessException, SecurityException, NoSuchMethodException,
			InstantiationException, InvocationTargetException {
		Class clazz = obj.getClass();
		Object obj1 = clazz.getConstructor().newInstance();
		Class clazz1 = obj1.getClass();
		Field fields[] = clazz.getDeclaredFields();
		Field fields1[] = clazz1.getDeclaredFields();

		for (int i = 0; i < fields.length; i++) {
			fields[i].setAccessible(true);
			fields1[i].setAccessible(true);
			Class t = fields1[i].getType();

			if (t.isPrimitive()) {
				fields1[i].set(obj1, fields[i].get(obj));
			} else if (!t.toString().equals("class java.lang.String")) {
				fields1[i].set(obj1, clone(fields[i].get(obj)));
			} else {
				fields1[i].set(obj1, fields[i].get(obj));
			}
		}
		return obj1;
	}

	public static void main(String[] args) throws IllegalArgumentException,
			IllegalAccessException, SecurityException, NoSuchMethodException,
			InstantiationException, InvocationTargetException {
		CatsHead head = new CatsHead(10, "white");
		Cat cat = new Cat("black", "Murka", head);

		Object cat1 = new Cat();

		cat1 = clone(cat);

		System.out.print("Cat 1 " + cat + "\n");
		System.out.print("Cat 2 " + cat1 + "\n");
		cat.setColor("red");
		System.out.print("Cat 1 " + cat + "\n");
		System.out.print("Cat 2 " + cat1 + "\n");
		cat.getCatsHead().setRadius(99);
		System.out.print("Cat 1 " + cat + "\n");
		System.out.print("Cat 2 " + cat1 + "\n");
	}

}
