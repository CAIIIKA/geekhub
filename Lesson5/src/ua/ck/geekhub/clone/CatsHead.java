package ua.ck.geekhub.clone;

import java.util.Arrays;

public class CatsHead {
	private int radius;
	private String color;

	public CatsHead(int radius, String color) {
		this.radius = radius;
		this.color = color;
	}
	
	public CatsHead() {
	}

	void setRadius(int radius) {
		this.radius = radius;
	}

	void setColor(String color) {
		this.color = color;
	}

	String getColor() {
		return color;
	}

	float getRadius() {
		return radius;
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 11 + radius;
		hash = hash * 11 + color.hashCode();
		return hash;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !(obj instanceof CatsHead)) {
			return false;
		}
		CatsHead obj1 = (CatsHead) obj;
	
		if (obj1.getColor().equals(radius)&&obj1.getRadius()==radius)
			return true;
		return false;
	}
	
	public String toString() {
		return " Radius of head= " + radius + " Head color= " + color;
	}
}
