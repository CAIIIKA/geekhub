package ua.ck.geekhub.clone;

public class Cat {

	private String color;
	private String name;
	private CatsHead catsHead;

	public Cat(String color, String name, CatsHead catsHead) {
		this.color = color;
		this.name = name;
		this.catsHead = catsHead;
	}
	
	public Cat() {
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCatsHead(CatsHead catsHead) {
		this.catsHead = catsHead;
	}

	public String getColor() {
		return color;
	}

	public String getName() {
		return name;
	}

	public CatsHead getCatsHead() {
		return catsHead;
	}

	@Override
	public String toString() {
		return "Cat colour: = " + color + " Cat name = " + name
				+ catsHead.toString();
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = hash * 11 + name.hashCode();
		hash = hash * 11 + color.hashCode();
		hash = hash * 11 + catsHead.hashCode();
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || !(obj instanceof Cat)) {
			return false;
		}
		Cat obj1 = (Cat) obj;

		if (obj1.getColor().equals(color) && obj1.getName().equals(name)
				|| obj1.getCatsHead().equals(catsHead))
			return true;
		return false;
	}
}
