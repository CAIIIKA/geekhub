package geekhub.components;

import org.springframework.stereotype.Component;

@Component
public class Horn implements StatusAware {
	String status;

	@Override
	public String status() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
