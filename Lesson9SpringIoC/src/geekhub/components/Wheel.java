package geekhub.components;

import org.springframework.stereotype.Component;

@Component
public class Wheel implements StatusAware {
	private String status;

	public void setStatus(String status) {
		this.status = status;
	}

	public String status() {
		return status;
	}

}