package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SteeringWheel {
	String status;
	
	@Autowired
	ControlPanel cp;

	@Autowired
	ForwardWheels fw;

	public String status() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		fw.stear(status);
	}
}
