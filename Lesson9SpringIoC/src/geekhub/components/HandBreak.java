package geekhub.components;

import org.springframework.stereotype.Component;

@Component
public class HandBreak implements StatusAware {
	private int power;
	private String status;

	public void setPower(int power) {
		this.power = power;
	}

	public int getPower() {
		return power;
	}

	public String status() {
		return status;
	}

	public void setStatus(String status) {
		if (status.equals("on"))
			power = 1;
		else
			power = 0;
		this.status = status;
	}
}
