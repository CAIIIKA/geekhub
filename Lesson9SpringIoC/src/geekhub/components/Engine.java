package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Engine implements StatusAware {
	String status;
	@Autowired
	Accelerator accelerator;

	@Autowired
	ControlPanel controlPanel;

	@Autowired
	GasTank gt;

	public void accelerate() {
		if (gt.getFlue() != 0)
			controlPanel.setSpeed(controlPanel.getSpeed() + accelerator.getPower());
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String status() {
		return status;
	}

}
