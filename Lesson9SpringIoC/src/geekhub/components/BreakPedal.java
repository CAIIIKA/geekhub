package geekhub.components;

import org.springframework.stereotype.Component;

@Component
public class BreakPedal implements StatusAware {
	private int power;
	private String status;

	public int getPower() {
		return power;
	}

	@Override
	public String status() {
		return status;
	}

	public void setStatus(String status) {
		if (status.equals("on"))
			power = 1;
		else
			power = 0;
		this.status = status;
	}
}
