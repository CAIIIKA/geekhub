package geekhub.components;

import org.springframework.stereotype.Component;

@Component
public class GasTank implements StatusAware {
	Integer flue;

	public String status() {
		return flue.toString();
	}

	public void setFlue(Integer flue) {
		if (flue < 0)
			flue = 0;
		this.flue = flue;
	}

	public Integer getFlue() {
		return flue;
	}
}
