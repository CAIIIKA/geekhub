package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Vehicle {
	@Autowired
	ControlPanel controlPanel;

	public ControlPanel getControlPanel() {
		return controlPanel;
	}

}
