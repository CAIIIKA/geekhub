package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ControlPanel implements StatusAware {
	Integer speed;
	@Autowired
	ForwardWheels forwardWheels;

	@Autowired
	RearWheels rearWheels;

	@Autowired
	HandBreak handBreak;

	@Autowired
	BreakPedal breakPedal;

	@Autowired
	Accelerator accelerator;

	@Autowired
	Engine engine;

	@Autowired
	SteeringWheel steeringWheel;

	@Autowired
	GasTank gasTank;

	@Autowired
	Horn horn;

	public void handRetard(String status) {
		handBreak.setStatus(status);
	}

	public void pedalRetard(String status) {
		breakPedal.setStatus(status);
	}

	public void accelerate(String status) {
		accelerator.setStatus(status);
	}

	public void steer(String way) {
		steeringWheel.setStatus(way);
	}

	public void horn(String status) {
		horn.setStatus(status);
	}

	public void init() {
		speed = 10;
		System.out.print("Speed: " + speed + "\n");

		horn.setStatus("off");
		System.out.print("Horn: " + horn.status() + "\n");

		breakPedal.setStatus("off");
		System.out.print("Break pedal: " + breakPedal.status() + "\n");

		handBreak.setStatus("off");
		System.out.print("Hand break: " + handBreak.status() + "\n");

		steeringWheel.setStatus("forward");
		System.out.print("Steering wheel: " + steeringWheel.status() + "\n");

		forwardWheels.setLeftStatus("Ok");
		forwardWheels.setRightStatus("Ok");
		rearWheels.setLeftStatus("Ok");
		rearWheels.setRightStatus("Ok");
		System.out.print("Forward Wheels: " + forwardWheels.status());
		System.out.print("Rear Wheels: " + rearWheels.status());

		engine.setStatus("Ok");
		System.out.print("Engine: " + engine.status() + "\n");

		gasTank.setFlue(200);
		System.out.print("Flue in gas tank: " + gasTank.status() + "\n\n");
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	public Integer getSpeed() {
		return speed;
	}

	@Override
	public String status() {
		return "Speed: " + speed + "\n" + "Horn: " + horn.status() + "\n"
				+ "Break pedal: " + breakPedal.status() + "\n" + "Hand break: "
				+ handBreak.status() + "\n" + "Steering wheel: " + steeringWheel.status() + "\n"
				+ "Forward Wheels: " + forwardWheels.status() + "Rear Wheels: "
				+ rearWheels.status() + "Engine: " + engine.status() + "\n"
				+ "Flue in gas tank: " + gasTank.status() + "\n\n";
	}

	public void oneSecond() {
		forwardWheels.retard();
		rearWheels.retard();
		engine.accelerate();
		gasTank.setFlue(gasTank.getFlue() - 1);
	}
}
