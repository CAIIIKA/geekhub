package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RearWheels implements StatusAware, Retarding {

	@Autowired
	Wheel leftWheel;

	@Autowired
	Wheel rightWheel;

	@Autowired
	ControlPanel controlPanel;

	@Autowired
	BreakPedal breakPedal;

	@Override
	public String status() {
		return "\nLeft Wheel: " + leftWheel.status() + "\n" + "Right Wheel: "
				+ rightWheel.status() + "\n";
	}

	@Override
	public void retard() {
		controlPanel.setSpeed(controlPanel.getSpeed() - breakPedal.getPower());
		if (controlPanel.getSpeed() < 0)
			controlPanel.setSpeed(0);

	}

	public void setLeftStatus(String status) {
		leftWheel.setStatus(status);
	}

	public void setRightStatus(String status) {
		rightWheel.setStatus(status);
	}

}
