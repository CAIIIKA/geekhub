package geekhub.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ForwardWheels implements StatusAware, Steared, Retarding {
	String way;
	
	@Autowired
	Wheel leftWheel;
	
	@Autowired
	Wheel rightWheel;

	@Autowired
	ControlPanel controlPanel;

	@Autowired
	HandBreak handBreak;

	@Autowired
	BreakPedal breakPedal;

	@Override
	public String status() {
		return "\nWay: " + way + "\n" + "Left Wheel: " + leftWheel.status()
				+ "\n" + "Right Wheel: " + rightWheel.status() + "\n";
	}

	@Override
	public void retard() {
		if (handBreak.getPower() == 1 && handBreak.getPower() == 1)
			controlPanel.setSpeed(controlPanel.getSpeed() - breakPedal.getPower());
		else
			controlPanel.setSpeed(controlPanel.getSpeed() - breakPedal.getPower() - handBreak.getPower());
		if (controlPanel.getSpeed() < 0)
			controlPanel.setSpeed(0);

	}

	@Override
	public void stear(String way) {
		this.way = way;
	}

	public String getWay() {
		return way;
	}

	public void setLeftStatus(String status) {
		leftWheel.setStatus(status);
	}

	public void setRightStatus(String status) {
		rightWheel.setStatus(status);
	}

}
