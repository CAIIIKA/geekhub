package geekhub;

import geekhub.components.ControlPanel;
import geekhub.components.Vehicle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class EntryPoint {
	public static void main(String [] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml"); 
		Vehicle vehicle = context.getBean(Vehicle.class);
		ControlPanel controlPanel = vehicle.getControlPanel();
		controlPanel.init();
		//cp.handRetard("on");
		//cp.pedalRetard("on");
		controlPanel.accelerate("on");
		for(int i = 0; i<3; i++){
			controlPanel.oneSecond();
			System.out.print(controlPanel.status());
		}
		controlPanel.horn("on");
		controlPanel.steer("right");
		controlPanel.accelerate("off");
		controlPanel.oneSecond();
		System.out.print(controlPanel.status());
		controlPanel.pedalRetard("on");
		controlPanel.oneSecond();
		System.out.print(controlPanel.status());
		
	}

}
