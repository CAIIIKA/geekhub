package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;

@Controller
public class UserController {
	
	@Autowired BaseDao dao;
	
	@RequestMapping("/listUsers.html")
	public String list(ModelMap map) {
		map.put("users", dao.list(User.class));
		return "users";
	}
	
	@RequestMapping("/loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		User user = id == null ? new User() : dao.get(User.class, id);
		map.put("user", user);
		return "user";
	}
	
	@RequestMapping("/deleteUser.html")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		dao.delete(User.class, id);
		return "redirect:./listUsers.html";
	}
	
	@RequestMapping("/saveUser.html")
	public String save(User user) {
		dao.save(user);
		return "redirect:./listUsers.html";
	}
}
