package ua.ck.geekhub.dz2;

import java.net.*;
import java.io.*;

class Client {

	public static void main(String[] args) {
		try {
			Socket clientSocket = new Socket("127.0.0.1", 1236);

			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
					true);
			BufferedReader inu = new BufferedReader(new InputStreamReader(
					System.in));

			String fuser, fserver;

			while ((fuser = inu.readLine()) != null) {
				out.println(fuser);
				fserver = in.readLine();
				System.out.println(fserver);

				if (fuser.equalsIgnoreCase("close")) {
					break;
				}
				if (fuser.equalsIgnoreCase("exit")) {
					break;
				}
			}

			out.close();
			in.close();
			inu.close();
			clientSocket.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}