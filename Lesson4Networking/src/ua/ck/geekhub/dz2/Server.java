package ua.ck.geekhub.dz2;

import java.net.*;
import java.io.*;

class Server {
	public static void main(String[] args) {
		try {
			ServerSocket server = null;
			Socket client;

			server = new ServerSocket(1236);
			client = server.accept();

			System.out.println("Connected");	 

			BufferedReader in = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			PrintWriter out = new PrintWriter(client.getOutputStream(), true);

			String input;

			while ((input = in.readLine()) != null) {
				if (input.equalsIgnoreCase("exit")) {
					break;
				}
				out.println("Server:" + input.substring(0, 1));
				System.out.println(input);
			}

			out.close();
			in.close();
			client.close();
			server.close();

			System.out.println("Server closed");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}