package ua.ck.geekhub.dz1;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import org.htmlparser.Parser;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

public class Connector {
	public static void main(String[] args) throws ParserException, IOException {
		String url = "https://www.google.ru/";
		List<String> lst = getLinksOnPage(url);
		List<String> lst1 = new ArrayList<String>();
		List<String> lst2 = new ArrayList<String>();
		List<String> lst3 = new ArrayList<String>();

		for (String s : lst) {
			HttpURLConnection urc = (HttpURLConnection) new URL(s)
					.openConnection();
			if (urc.getResponseCode() >= 200 && urc.getResponseCode() < 300)
				lst1.add(s);
			if (urc.getResponseCode() >= 300 && urc.getResponseCode() < 400)
				lst2.add(s);
			if (urc.getResponseCode() >= 400 && urc.getResponseCode() < 600)
				lst3.add(s);
		}
		System.out.print("Working links:\n");
		for (String s : lst1)
			System.out.print(s + "\n");
		System.out.print("Redirected links:\n");
		for (String s : lst2)
			System.out.print(s + "\n");
		System.out.print("Doesn't working links:\n");
		for (String s : lst2)
			System.out.print(s + "\n");

	}

	public static List<String> getLinksOnPage(final String url)
			throws ParserException {
		final Parser htmlParser = new Parser(url);
		final List<String> result = new LinkedList<String>();

		try {
			final NodeList tagNodeList = htmlParser
					.extractAllNodesThatMatch(new NodeClassFilter(LinkTag.class));
			for (int j = 0; j < tagNodeList.size(); j++) {
				final LinkTag loopLink = (LinkTag) tagNodeList.elementAt(j);
				final String loopLinkStr = loopLink.getLink();
				result.add(loopLinkStr);
			}
		} catch (ParserException e) {
			e.printStackTrace();
		}

		return result;
	}

}
