
<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c"%>

<html>
<head>
<title>Session</title>
<style type="text/css">
body,table,hr {
	color: black;
	background: silver;
}
</style>
</head>
<body>
	<form action="./session">
		<table border="1">
			<tr>
				<td>Message:</td>
				<td><input type="text" size="30" name="message"></td>
			</tr>

			<tr>
				<td>Action:</td>
				<td><input type="radio" name="action" value="add"
					checked="checked">add<br> <input type="radio"
					name="action" value="update">update<br> <input
					type="radio" name="action" value="remove">remove<br> <input
					type="radio" name="action" value="invalidate">clear</td>
			</tr>

			<tr>
				<td>ServletContext or Session:</td>
				<td><input type="radio" name="actionType" value="session"
					checked="checked">Session<br> <input type="radio"
					name="actionType" value="context">ServletContext<br></td>
			</tr>

			<tr>
				<td>Name:</td>
				<td><input type="text" size="30" name="name"></td>
			</tr>

			<tr>
				<td>Value:</td>
				<td><input type="text" size="30" name="value"></td>
			</tr>
		</table>
		<br> <input type="submit" name="result" value="Result">
	</form>
	<c:out value="Message: ${message}" />
	<br>
	<br>
	<c:out value="Session:" />
	<br>
	<br>
	<table border="1">
		<tr>
			<td><c:out value="Name: " /></td>
			<td><c:out value="Value: " /></td>
		</tr>

		<!--  
		<c:forEach var="name" items="${pageContext.session.attributeNames}">
			<tr>
				<td><c:out value="${name}" /></td>
				<td><c:out value="${sessionScope[name]}" /></td>
			</tr>
			
		</c:forEach>
		-->

		<c:forEach items='${sessionScope}' var='p'>
			<tr>
				<td><c:out value='${p.key}' /></td>
				<td><c:out value='${p.value}' /></td>
			</tr>
		</c:forEach>

	</table>
	<br>
	<c:out value="ServletContext:" />
	<br>
	<br>
	<table border="1">
		<tr>
			<td><c:out value="Name: " /></td>
			<td><c:out value="Value: " /></td>
		</tr>

		<!--
		<c:forEach var="attr" varStatus="status"
			items="${servletContextAttribute}">
			<tr>
				<td><c:out value="${attr}" /></td>
				<td><c:out value="${servletContextValue[status.index]}" /></td>
			</tr>
		</c:forEach>
		-->
		<%--c:forEach items='${applicationScope}' var='p'>
			<tr>
				<td><c:out value='${p.key}' /></td>
				<td><c:out value='${p.value}' /></td>
			</tr>
		</c:forEach--%>
		<c:forEach var="name"
			items="${pageContext.request.servletContext.attributeNames}">
			<tr>
				<td><c:out value="${name}" /></td>
				<td><c:out value="${applicationScope[name]}" /></td>
			</tr>
		</c:forEach>

	</table>
</body>
</html>