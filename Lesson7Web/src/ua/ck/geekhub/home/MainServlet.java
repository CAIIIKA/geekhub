package ua.ck.geekhub.home;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MainServlet
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		String actionType = request.getParameter("actionType");
		String message = request.getParameter("message");

		HttpSession session = request.getSession();
	
		/*
		 * There is Radio Button in the main.jsp page and it is always checked
		 */
		if (action == null) {
			// send data to the page when action = null(first launch)
			getServletContext().getRequestDispatcher("/main.jsp").forward(
					request, response);
			return;
		}
		if (actionType.equals("session")) {
			if (action.equals("add")) {
				session.setAttribute(name, value);
			} else if (action.equals("update")) {
				session.setAttribute(name, value);
			} else if (action.equals("remove")) {
				session.removeAttribute(name);
			} else if (action.equals("invalidate")) {
				session.invalidate();
				getServletContext().getRequestDispatcher("/main.jsp").forward(
						request, response);
				return;
			}
		} else {
			if (action.equals("add")) {
				request.getServletContext().setAttribute(name, value);
			} else if (action.equals("update")) {
				request.getServletContext().setAttribute(name, value);
			} else if (action.equals("remove")) {
				request.getServletContext().removeAttribute(name);
			} else if (action.equals("invalidate")) {
				Enumeration<String> servletContextAttr = request
						.getServletContext().getAttributeNames();
				while (servletContextAttr.hasMoreElements()) {
					String attrName = (String) servletContextAttr.nextElement();
					request.getServletContext().removeAttribute(attrName);
				}
				request.setAttribute("message", message);
				getServletContext().getRequestDispatcher("/main.jsp").forward(
						request, response);
				return;
			}
		}
		request.setAttribute("message", message);
		getServletContext().getRequestDispatcher("/main.jsp").forward(request,
				response);
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
