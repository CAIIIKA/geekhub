package ua.ck.geekhub.home;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MainServlet
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");

		if (action == null) {
			getServletContext().getRequestDispatcher("/main.jsp").forward(
					request, response);
			return;
		}

		if (action.equals("add")) {
			Enumeration<String> atr = session.getAttributeNames();
			while (atr.hasMoreElements()) {
				if (atr.nextElement().equals(name)) {
					@SuppressWarnings("unchecked")
					List<String> sesAtr = (ArrayList<String>) session
							.getAttribute(name);
					sesAtr.add(value);
					session.setAttribute(name, sesAtr);
					getServletContext().getRequestDispatcher("/main.jsp")
							.forward(request, response);
					return;
				}
			}
			List<String> sesAtr = new ArrayList<String>();
			sesAtr.add(value);
			session.setAttribute(name, sesAtr);
		}
		if (action.equals("remove")) {
			@SuppressWarnings("unchecked")
			List<String> sesAtr = (ArrayList<String>) session
					.getAttribute(name);
			sesAtr.remove(value);
			if(sesAtr.isEmpty())
				session.removeAttribute(name);
			else
				session.setAttribute(name, sesAtr);
			getServletContext().getRequestDispatcher("/main.jsp").forward(
					request, response);
			return;
		}

		getServletContext().getRequestDispatcher("/main.jsp").forward(request,
				response);
		return;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
