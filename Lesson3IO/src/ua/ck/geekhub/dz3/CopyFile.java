package ua.ck.geekhub.dz3;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CopyFile {
	File iFile;
	File oFile;

	public CopyFile(File iFile, File oFile) {
		this.iFile = iFile;
		this.oFile = oFile;
	}

	public long useBufferedStream() throws IOException {

		long time1 = System.currentTimeMillis();
		InputStream is = new FileInputStream(iFile);
		FileOutputStream os = new FileOutputStream(oFile);
		BufferedInputStream bis = new BufferedInputStream(is);
		BufferedOutputStream bos = new BufferedOutputStream(os);

		while (true) {
			int data = bis.read();
			if (data == -1)
				break;
			bos.write(data);
		}
		bos.flush();
		os.close();
		is.close();
		bis.close();
		bos.close();
		long time2 = System.currentTimeMillis();
		return time2 - time1;
	}

	public long useBuffer() throws IOException {

		long time1 = System.currentTimeMillis();
		InputStream is = new FileInputStream(iFile);
		FileOutputStream os = new FileOutputStream(oFile);
		byte[] buf = new byte[64 * 1024];
		int len = 0;
		while ((len = is.read(buf)) != -1) {
			os.write(buf, 0, len);
		}
		os.flush();
		os.close();
		is.close();
		long time2 = System.currentTimeMillis();
		return time2 - time1;
	}

	public long withoutBuffer() throws IOException {
		long time1 = System.currentTimeMillis();
		InputStream is = new FileInputStream(iFile);
		FileOutputStream os = new FileOutputStream(oFile);
		int tmp = is.read();
		while (tmp != -1) {
			os.write(tmp);
			tmp = is.read();
		}
		os.flush();
		os.close();
		is.close();
		long time2 = System.currentTimeMillis();
		return time2 - time1;
	}
}
