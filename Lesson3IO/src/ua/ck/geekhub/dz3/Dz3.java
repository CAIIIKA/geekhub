package ua.ck.geekhub.dz3;

import java.io.File;

public class Dz3 {
	public static void main(String[] args) throws Exception {
		File iFile = new File(args[1]);
		File oFile = new File(args[2]);

		CopyFile a = new CopyFile(iFile, oFile);
		if (args[0].equals("1"))
			a.useBuffer();
		if (args[0].equals("2"))
			a.withoutBuffer();
		if (args[0].equals("3"))
			a.useBufferedStream();
	}

}