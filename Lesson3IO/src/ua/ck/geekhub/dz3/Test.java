package ua.ck.geekhub.dz3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Test {
	public static void main(String[] args) throws Exception {
		File iFile = new File("D:\\World-of-Warcraft-Setup-ruRU.exe");
		File oFile = new File("D:\\tmp\\World-of-Warcraft-Setup-ruRU.exe");

		CopyFile a = new CopyFile(iFile, oFile);
		System.out.println("Use buffer: " + a.useBuffer());
		System.out.println("Use buffereed stream: " + a.useBufferedStream());
		System.out.println("Without buffer: " + a.withoutBuffer());
	}

}