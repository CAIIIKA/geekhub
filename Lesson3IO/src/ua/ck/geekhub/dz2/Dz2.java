package ua.ck.geekhub.dz2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Dz2 {
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		List<String> lst = new ArrayList<String>();
		lst.add("Cp1251");
		lst.add("Cp866");

		System.out.println("Choose character encoding: ");
		Iterator it = lst.iterator();
		while (it.hasNext()) {
			String element = (String) it.next();
			System.out.println(element);
		}

		Reader reader = new InputStreamReader(new FileInputStream("D:\\1.txt"));
		Writer writer = new OutputStreamWriter(
				new FileOutputStream("D:\\2.txt"), br.readLine());
		int c = 0;
		while ((c = reader.read()) >= 0)
			writer.write(c);
		reader.close();
		writer.close();
	}

}
