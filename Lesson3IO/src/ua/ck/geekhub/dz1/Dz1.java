package ua.ck.geekhub.dz1;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dz1 {
	public static void main(String[] args) throws IOException {
		if (args[0].equals("1")) {
			File file = new File(args[1]);
			if (file.exists()) {
				System.out.println("Folder exists");
				System.exit(0);
			}
			if (file.mkdir()) {
				System.out.println(file + " was created");
			}
		}

		else if (args[0].equals("2")) {
			String b = args[1];
			String c = args[2];
			File oldFile = new File(b);
			if (oldFile.exists()) {
				oldFile.renameTo(new File(c));
				System.out.println("rename was succesfull");
			} else {
				System.out.println("Folder does not exist");
				System.exit(0);
			}
		}

		else if (args[0].equals("3")) {
			File file = new File(args[1]);
			if (!file.exists()) {
				System.out.print("File does not exists");
				System.exit(0);
			}
			if (file.delete())
				System.out.println("Folder deleted.");
			else
				System.out.println("Failed");
		}
	}
}
